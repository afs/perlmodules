# Vos.pm - parses output of vos commands and provides functions to access
#          it in a more clean way. In a future version it will check for
#          the existence of AFS.pm (>=2.2) and call functions from that
#          module to avoid parsing the vos command output
#
#
# Wolfgang Friebel 11/2001 (latest update Sep 17, 2002)
# Bernard Antoine  03/2003 get rid of (some) "undefined variable" messages
#                  06/2003 vos listvldb MAY return sites in strange order
# Wolfgang Friebel 02/2004 vos listvldb and examine handle now RO only! vols
#                          updated accesses and rw_site accordingly
# Bernard Antoine  02/2004 better support for cached "committed" data
#                  03/2004 cache list of fs servers (vos listaddrs called far
#                          too often)
# Wolfgang Friebel 01/2006 merge CERN and DESY version, new methods of parsing
#                          vos command output, bug fixes.
# Bernard Antoine  05/2006 several fixes to Desy's code
# (and see RCS/GIT logs)
package Vos;

use strict;
use Exporter;
use vars qw (@ISA @EXPORT $VERSION %Vols %Part $servers
             $legal_username_pattern $legal_nodename_pattern
             $legal_volname_pattern
            );
@ISA = qw(Exporter);
@EXPORT = qw (exa_single_volumes set_cache_time examine vos_listvol fullpath
              exa_use_ext iocounts
              $legal_username_pattern $legal_nodename_pattern
              $legal_volname_pattern
              afsvol_server
              vos_listvldb vos_partinfo get_partattrib get_volattrib quota
              blocks accesses last_update volumes vid2name volid get_fsservers
              afsvol_part rw_site disk_capacity disk_used
              free_percent new_committed partitions vos_execute noaction
              purge_diskinfo purge_volinfo purge_committed
              _print 
             );
$VERSION = '1.13';

$legal_username_pattern = '[a-z][\w\-]+';  # a-z then a-zA-Z_0-9-  ( . excluded )
$legal_nodename_pattern = '[a-z][\w\-]+';  # ...      a-zA-z_0-9-
$legal_volname_pattern  = '[a-zA-Z][\w\-\.]+'; # ...     a-zA-z_0-9-. ( . included )

### Rules for parsing vos ... output
###   The rules are stored as rows of an anonymous array. Elements in the row:
###   0    - pattern to be matched, on success currentstate is set to state
###   1    - state, determines if and when a rule is tried
###          0 - rule is always tried
###          n - rule is tried if there was no match yet for current line and
###              currentstate <= state or
###              if there was a match and currentstate == state
###   2      @array: store matched values in hash attribute array (anon array)
###   2..n - string: store matched value in hash using string as attribute
###
# parse phrases valid for vos examine vos listvol and vos listvldb
my $vos_listvol =  [['^(\S+)\s*(\d+)\s*(RW|RO|BK)\s*(-?\d+)\s*K\s+(?:used (\d+) files (.*)|(.*))', 0, qw(Name VolID Voltype Blocks Files Status Status)],
                    # skip empty lines
                    ['^\s*$', 0],
                    ['^Total number of volumes on server', 0],
                    # error messages in vos listvol
                    ['Volume\s+(\d+)\s+is\s+(busy)', 0, qw(VolID Status)],
                    ['Could (not attach) volume\s+(\d+)', 0, qw(Status VolID)],
                    ['^\s+(\S+)\s+\/vicep(\w+)\s*$', 2, qw(Server Vicep)],
                    ['^\s+RWrite\s', 3],
                    ['^\s+MaxQuota\s*(\d+)\s*K', 4, 'Quota'],
                    ['^\s+Creation\s+(.*)', 5, 'Created'],
                    ['^\s+Copy\s+(.*)', 6, 'Copied'],
                    ['^\s+Backup\s+', 7],
                    ['^\s+Last Access\s+', 8],
                    ['^\s+Last Update\s+(.*)', 9, 'Updated'],
                    ['^\s+(\d+)\s+accesses in the past day', 10, 'Access'],
                    ['Raw Read|Writes Affecting|\s\|', 12],
                    # collect summary info, will be moved away in vos_listvol
                    ['Total volumes:\s+(\d+)\s+on-line,\s+(\d+)\s+off-line,\s+(\d+)\s+busy', 21, qw(Online Offline Busy)],
                    ['Total volumes onLine\s+(\d+).*offLine\s+(\d+).*busy\s+(\d+)', 22, qw(Online Offline Busy)],
                  ];
my $vos_examine =  [['^(\S+)\s*(\d+)\s*(RW|RO|BK)\s*(-?\d+)\s*K\s+(?:used (\d+) files (.*)|(.*))', 0, qw(Name VolID Voltype Blocks Files Status Status)],
                    # skip empty lines
                    ['^\s*$', 0],
                    ['^VLDB: no such entry', 0],
                    ['Volume\s+(\d+)\s+is\s+(busy)', 0, qw(VolID Status)],
                    ['Could (not attach) volume\s+(\d+)', 0, qw(Status VolID)],
                    ['^\s+(\S+)\s+\/vicep(\w+)\s*$', 2, qw(Server Vicep)],
                    ['^\s+RWrite\s', 3],
                    ['^\s+MaxQuota\s*(\d+)\s*K', 4, 'Quota'],
                    ['^\s+Creation\s+(.*)', 5, 'Created'],
                    ['^\s+Copy\s+(.*)', 6, 'Copied'],
                    ['^\s+Backup\s+', 7],
                    ['^\s+Last Access\s+', 8],
                    ['^\s+Last Update\s+(.*)', 9, 'Updated'],
                    ['^\s+(\d+)\s+accesses in the past day', 10, 'Access'],
                    ['^\s+RWrite:\s+(\d+)', 11, 'RWVol'],
                    ['\s+ROnly:\s+(\d+)', 11, 'ROVol'],
                    ['\s+Backup:\s+(\d+)', 11, 'BKVol'],
                    ['Raw Read|Writes Affecting|\s\|', 12],
                    ['^\s+number of sites ->\s+(\d+)', 13, 'Sites'],
                    ['^\s+server (\S+) partition \/vicep(\w+) (RW|RO|BK) Site(.*)', 14, '@Partition'],
                    ['Volume is currently (LOCKED)', 19, 'Locked'],
                    ['Volume is locked for a (\S+) operation', 20],
                    # the line below is for compatibility with vos listvldb
                    ['^(\S+)\s*$', 1, 'Name'],
                  ];
# separate definition for vos listvldb to speed up parsing
my $vos_listvldb = [['^(\S+)\s*$', 0, 'Name'],
                    ['^\s*$', 0],
                    ['^VLDB: no such entry', 0],
                    ['^\s+RWrite:\s+(\d+)', 8, 'RWVol'],
                    ['\s+ROnly:\s+(\d+)', 8, 'ROVol'],
                    ['\s+Backup:\s+(\d+)', 8, 'BKVol'],
                    ['^\s+number of sites ->\s+(\d+)', 13, 'Sites'],
                    ['^\s+server (\S+) partition \/vicep(\w+) (RW|RO|BK) Site(.*)', 14, '@Partition'],
                    ['Volume is currently (LOCKED)', 19, 'Locked'],
                    ['Volume is locked for a (\S+) operation', 20],
                   ];

my $opt_n=0;
my $opt_v=0;
my $Cache_time = 24*3600;
my $vos = "vos";

my $AFSVOL_SINGLE = 0;
my $USE_EXT       = 0;

# defer initialization - do not run code on compilation
INIT {
  $vos = fullpath("vos",
                  "/usr/sbin:/usr/sue/bin/:/usr/bin");
  select STDERR; $| = 1;
  select STDOUT; $| = 1;
}

# some print related routines
sub _print {
  my($how, $mysub, $myzub, $txt);
  if    ($_[0] =~ /^E(?:RROR)?$/) { shift @_; $how = "ERROR"; }
  elsif ($_[0] =~ /^W(?:ARN)?$/)  { shift @_; $how = "WARN"; }
  elsif ($_[0] =~ /^D(?:IE)?$/)   { shift @_; $how = "DIE"; }
  else {                                      $how = ""; }
  $txt = join "", @_;
  if (defined $opt_v && $opt_v) {
    my ($z,$a,@a) = ('','','');
    my $i = (@_) ? 1 : 0;
    @a=caller($i++); ($a = $a[3]) =~ s/.*::_?_?//;
    if ($a =~ /_exec/) {
      @a=caller($i++); ($a = $a[3]) =~ s/.*::_?_?//;
    }
    while (@a=caller($i++)) { $z .= " "; }
    $mysub = "$z$a: ";
    ($myzub = $mysub) =~ s/./ /g;
    $txt =~ s/^(\t)/$myzub/gs;
    $txt =~ s/(\n\t)/\n$myzub\t/gs;
  } else {
    $mysub = "";
  }

  if ($how eq "ERROR") {
    warn "${mysub}ERROR: $txt\n";
  } elsif ($how eq "WARN") {
    warn "${mysub}WARN: $txt\n";
  } elsif ($how eq "DIE") {
    die "${mysub}FATAL: $txt\n";
  } else {
    print "$mysub$txt\n";
  }
}


sub get_fsservers {
  my $now = time();
# if Cache_time passed or info not available then call vos listaddrs
  if ( (defined $servers and $servers->{_time} + $Cache_time < $now )
       or ( ! defined $servers or ! defined $servers->{servers} )) {
    (my $srv = vos_execute('listaddrs -noauth')) =~ s/\..*$//mg;
    $servers = {};
    @{$servers->{servers}} = sort grep {$_ ne '<unknown>'} split /\n/, $srv;
    $servers->{_time} = $now;
  }
  return @{$servers->{servers}};
}

sub noaction {
  @_ ? ($opt_n = shift) : return $opt_n;
}

sub verbose {
  my $old_opt_v = $opt_v;
  $opt_v = shift;
  return $old_opt_v;
}

sub badargs {
  my $res = $_[0]; shift @_;
  _print 'E', "command @_ gave the following error\n$res" if $opt_v;
  return undef;
}

sub exa_single_volumes {
  my $p =$AFSVOL_SINGLE;
  $AFSVOL_SINGLE = shift;
  return $p;
}

sub exa_use_ext {
  my $p =$USE_EXT;
  $USE_EXT = shift;
  return $p;
}

sub set_cache_time {
  $Cache_time = shift;
}

sub quota {
  get_volattrib ($_[0], 'Quota');
}

sub blocks {
  get_volattrib ($_[0], 'Blocks');
}

sub accesses {
  my $res = get_volattrib ($_[0], 'Accesses');
  return unless $res;
  if ( $_[1] ) {
    return $res->{$_[1]} if exists $res->{$_[1]};
    return undef
  } else {
    return %$res;
  }
}

sub last_update {
  get_volattrib ($_[0], 'Updated');
}

sub volumes {
# call vos listvol with option -fast
  my $arg0 = shift;
  local $_; # do not clobber the global $_
  if ( $arg0 ) {
    unshift @_, split('/', $arg0);
    $_=vos_execute ("listvol @_ -fast -quiet -noauth");
    return undef if $?;
    my @vols = split /\s*\n/, $_;
    return @vols;
  } else {
    $_ = vos_execute("listvldb -noauth -quiet");
    return undef if $?;
    my @vols = /^\S+/mg;
    return @vols;
  }
}
    
sub vid2name {
  return $_[0] if $_[0] !~ /^\d+$/; #is already a name
  get_volattrib ($_[0], 'Name');
}

sub volid {
  return $_[0] if $_[0] =~ /^\d+$/; #is already an ID
  get_volattrib ($_[0], 'VolID');
}

sub afsvol_server {
  my $p = get_volattrib ($_[0], 'Partition');
  $p =~ s/\/.*$// if defined $p;
  return $p;
}

sub afsvol_part {
  my $res = get_volattrib ($_[0], 'Partition');
  return unless $res;
  if ( $_[1] ) {
    return (grep { /^$_[1]\// } @$res)[0];
  } else {
    return @$res;
  }
}

sub iocounts {
  my @res = ();
  my @part = @{get_volattrib($_[0],'Partition')};
  if (my $a = get_volattrib($_[0],'RWVol')) {
     push @res, ${get_volattrib($a,'Accesses')}{$part[0]};
  } else {
     push @res, undef;
  }
  if (my $a = get_volattrib($_[0],'ROVol')) {
     my %x = %{get_volattrib($a,'Accesses')};
     foreach (1 ... $#part ) {
         push @res, $x{$part[$_]};
     }
  }
  return @res;
}

sub rw_site {
  my $name = vid2name($_[0]);
  $name =~ s/\.backup$|\.readonly$//;
  my $res = get_volattrib ($name, 'Accesses');
  my @k = keys %$res;
# if there is only one entry, take that
  return $k[0] if $#k == 0;
# otherwise look up the partition info
  my $rw = get_volattrib ($name, 'Partition');
  return ($rw->[0]) if defined($rw);
  return undef;

}

sub disk_capacity {
  get_partattrib (@_, 'Total');
}

sub disk_used {
  get_partattrib (@_, 'Used');
}

sub free_percent {
  get_partattrib (@_, 'PercentFree');
}

sub committed {
  get_partattrib (@_, 'Committed');
}

sub partitions {
  return @{$Part{$_[0]}} if exists $Part{$_[0]} and $_[0] !~ /\//;
  my @parts = vos_partinfo (@_);
  return map { $_->{Partition} } @parts;
}

sub purge_diskinfo {
  delete $Part{$_[0]};
}

sub purge_volinfo {
  my $vname = vid2name($_[0]);
  if ( $vname ) {
    $vname =~ s/\.readonly$//;
    delete $Vols{$vname};
    delete $Vols{"$vname.readonly"};
  }
  my $vid = volid($_[0]);
  if ( $vid ) {
    if ( exists $Vols{$vid}->{ROVol} ) {
      my $vidro = $Vols{$vid}->{ROVol};
      $vid = $Vols{$vid}->{RWVol} if $vid == $vidro;
      delete $Vols{$vidro};
    }
    delete $Vols{$vid};
  }
}

sub get_partattrib {
  my ($key, $attr) = @_;
  return unless $key;
# the first argument is a server/partition pair separated by slash
  my @prti = qw( Partition PercentFree Used Total );
  my @lvol = qw( onLine offLine busy Volumes );
  my @comm = qw( Committed );
  my $now = time();
# check if partition attibute well formed
  if ( grep {$_ eq $attr} @prti and $key !~ /\// ) {
    _print 'E', "malformed partition name (without /)";
    return undef;
  }
# if Cache_time passed or info not available then call vos listvol
  if ( (exists $Part{$key} and $Part{$key}->{_time} + $Cache_time < $now )
       or ( ! exists $Part{$key} or ! exists $Part{$key}->{$attr} )) {
    if ( grep {$_ eq $attr} @prti ) {
      vos_partinfo ($key);
    } elsif ( grep {$_ eq $attr} @lvol ) {
      vos_listvol ($key);
    } elsif ( grep {$_ eq $attr} @comm ) {
      part_committed ($key);
    } else {
      _print 'E', "invalid attribute name $attr"
           unless $attr eq '_time';
      return undef;
    }
  } 
  return $Part{$key}->{$attr}
        if exists $Part{$key} and exists $Part{$key}->{$attr};
  _print 'E', "attribute $attr for partition $key not found"
        if exists $Part{$key} and ! exists $Part{$key}->{$attr};
  _print 'E', "partition $key not found" unless exists $Part{$key};
  return undef;
}

sub get_volattrib {
  my $vol = $_[0];
  return unless $vol;
  if ( ! $_[1] ) {
    _print 'E', "missing attribute name";
    return undef;
  }
  my @accs = qw( Accesses );
  my @ext  = qw( Files );
  my @exam = qw( Blocks Status Quota Created Updated Copied );
  my @vldb = qw( Partition Name VolID Voltype Sites RWVol ROVol BKVol ReleaseError);
  my $now = time();
  # it may happen that a volume is not created "enough" to be visible by vos listvol or vos exam,
  # but is however visible to vos listvol -fast. If we detect that, we have better pretend it
  # never existed
  if ( exists $Vols{$vol} and ! (exists $Vols{$vol}->{_time} and $Vols{$vol}->{_time} =~ /\d+/)) {
    _print 'W', "_time not defined or not a number for $vol ($Vols{$vol}->{Name}): $Vols{$vol}->{_time}" if $opt_v;
    delete $Vols{$vol};
    return undef;
  }
  if ( (exists $Vols{$vol} and $Vols{$vol}->{_time} + $Cache_time < $now )
       or ( ! exists $Vols{$vol} ) or ( ! exists $Vols{$vol}->{$_[1]} )) {
# attributes that are returned only with vos examine or vos listvol
    if ( grep {$_ eq $_[1]} @ext  ) {
      $USE_EXT = 1;
      examine ($vol);
    } elsif ( grep {$_ eq $_[1]} @exam ) {
      examine ($vol);
    } elsif ( grep {$_ eq $_[1]} @accs ) {
# always use vos examine for 'Accesses' attribute
      vos_examine ($vol);
# attributes that can be found in the vos listvldb output
    } elsif ( grep {$_ eq $_[1]} @vldb ) {
      vos_listvldb ($vol);
    } else {
      _print 'E', "invalid attribute name $_[1]"
           unless $_[1] eq '_time';
      return undef;
    }
  } 
  return $Vols{$vol}->{$_[1]}
	 if exists $Vols{$vol} and exists $Vols{$vol}->{$_[1]};
# this is intended: return undefined if attribute not set (e.g. no BKVol)
#  _print 'E', "attribute $_[1] for volume $_[0] not found"
#	if defined $Vols{$vol} and ! defined $Vols{$vol}->{$_[1]};
#  _print 'E', "volume $_[0] not found" unless defined $Vols{$vol};
  return;
}

# Note on caching the "committed" data.
#
# done the hard way (loop through all volumes on the partition),
#  it costs an arm and a leg, as they say.
# but caching the information necessitates more code whose place is not
#  in a portable module like this one.
# consequently, we just test here if another module (CacheCommitted) is
# being "used" by our caller.
sub part_committed {
  my($key)=@_;
# code for cached info on Committed relies on another package CacheCommitted
# if available
  return if (defined(&CacheCommitted::cache_committed)) &&
      ($Part{$key}->{Committed} = CacheCommitted::cache_committed($key) );
# if not available, then we go the hard way , i.e. analyse vos listvol
  _print "Building 'committed' the hard way" if $opt_v;
  vos_listvol ($key);           # which also calls cache_committed
# this is faster (one vos listvol <part> than the loop found in the old pv.arc
#    my($comm)=0;
#    foreach (volumes($key)) {
#      next if ( $f = vid2name($_) ) =~ /.backup$/;
#      next if ( $f = vid2name($_) ) =~ /.readonly$/;
#      $comm += quota($_);
#    }
#    $Part{$key}->{Committed} = $comm;
}

sub new_committed {
  &part_committed($_[0]) unless defined $Part{$_[0]};
  $Part{$_[0]}->{Committed} += $_[1] if defined $Part{$_[0]};
# code for cached info on Committed relies on another package CacheCommitted
# if available
  CacheCommitted::cache_committed($_[0],$Part{$_[0]}->{Committed})
    if (defined(&CacheCommitted::cache_committed));
}

sub purge_committed {
  delete $Part{$_[0]}->{Committed} if exists $Part{$_[0]};
# code for cached info on Committed relies on another package CacheCommitted
# if available
  CacheCommitted::cache_committed($_[0],$Part{$_[0]}->{Committed})
    if (defined(&CacheCommitted::cache_committed));
}

sub examine {
#
# call vos_examine or vos_listvol depending on content of $AFSVOL_SINGLE
#
# arguments   : volid or volname
# return value: array of volume descriptions (representation: anonymous hash)
#
  my @res = ();
  if ( $AFSVOL_SINGLE ) {
    return vos_examine (@_);
  } else {
    my $vol = $_[0];
    vos_listvldb ($vol);
    # we have to loop over all partitions defined for this volume
    if ( exists $Vols{$vol} and exists $Vols{$vol}->{Partition} ) {
      for my $part (@{$Vols{$vol}->{Partition}}) {
         next unless $part;
         # do process partitions only once
         next if exists $Part{$part}->{onLine};
         push @res, vos_listvol ($part);
      }
    } else {
      return undef;
    }
    return @res;
  }
}

sub vos_partinfo {
#
# parse the output of vos partinfo
#
# arguments   : server [partition]
# return value: array of partition descriptions (representation: anonymous hash)
  my @parts = ();
# do allow also the server/partition notation (one argument only)
  (local $_="@_") =~ s/\// /;
  my @args = split /\s/, $_;
  if ( $#args == 0  || $#args == 1 ) {
    $_ = vos_execute("partinfo @args -noauth");
  } else {
    _print 'D', "wrong number of args (", $#args+1, "), should be 1 or 2";
  }
  (my $srv = $args[0]) =~ s/\..*$//;
# do fill the partition list if vos partinfo is called for a whole server
  $Part{$srv} = [] if $#args == 0;
  while (/partition \/vicep(\w+):\s+(\d+)\s+K blocks out of total (\d+)/cmg) {
    my ($part, $free, $total) = ($1, $2, $3);
    my $key = "$srv/$part";
    push @{$Part{$srv}}, $key if $#args == 0;
    my $freeperc = 0;
    my ($pdesc);
    $freeperc = int(100*$free/$total + 0.5) if $total;
    if ( exists $Part{$key} ) {
      $pdesc = $Part{$key};
    } else {
      $pdesc = {};
    }
    $pdesc->{Partition} = $key;
    $pdesc->{PercentFree} = $freeperc || 0;
    $pdesc->{Used} = $total - $free;
    $pdesc->{Total} = $total;
    $pdesc->{_time} = time();
    push @parts, $pdesc;
  }
  for (@parts) {
    $Part{$_->{Partition}} = $_;
  }
  return @parts;
}

sub vos_normalize {
# transform vos_parse hash into form that is used inside Vos package
    my $res = shift;
    for my $h ( @$res ) {
        # for a busy or not attached volume consult the vldb or the cache
        if ( exists $h->{VolID} and exists $h->{Status} and
             ($h->{Status} eq 'busy' or $h->{Status} eq 'not attach') ) {
          my $id = $h->{VolID};
          my @vldb = vos_listvldb($id);
          if (undef @vldb) {
            _print 'W', "cannot find information about volume $id" if $opt_v;
          }
          for my $vldb ( @vldb ) {
              next unless $vldb->{VolID} != $id;
              for ( keys %$vldb ) {
                  $h->{$_} = $vldb->{$_};
              }
              last; 
          }
        }
        # Accesses becomes a hash with one entry "server/part => number"
        if ( exists $h->{Server} and exists $h->{Vicep} ) {
            unless ( exists $h->{Access} ) { $h->{Access} = 0; }
            (my $srv = $h->{Server}) =~ s/\..*$//;
            my $key = "$srv/$h->{Vicep}";
            $h->{Accesses}->{$key} = $h->{Access};
            delete $h->{Server};
            delete $h->{Vicep};
            delete $h->{Access};
        } else {
                delete $h->{Access} if exists $h->{Access};
        }
        # Partition becomes an array with strings "server/part", RW always first
        if ( exists $h->{Partition} ) {
            my $has_rw = 0;
            for my $a ( @{$h->{Partition}} ) {
                $has_rw = 1 if $a->[2] eq 'RW';
                (my $err = $a->[3]) =~ s/^\s+-?-?\s*//;
                push @{$h->{ReleaseError}}, $err || undef;
                (my $srv = $a->[0]) =~ s/\..*$//;
                $a = "$srv/$a->[1]";
            }
            unshift @{$h->{Partition}}, '' if ! $has_rw;
            unshift @{$h->{ReleaseError}}, undef if ! $has_rw;
        }
        # cope with bug in old AFS version
        if ( exists $h->{Blocks} ) {
            $h->{Blocks} = (0x3fffff & $h->{Blocks}) - 1 if $h->{Blocks} < 0;
        }
        # another game where one never wins:
        # historically, vos create filled the "Creation" field
        #  then they invented "Copy" and vos create now fills both
        #  but in between, some versions (e.g. 1.3.73) filled "Copy" only!
        if ( exists $h->{Created} && $h->{Created} eq 'Thu Jan  1 01:00:00 1970' ) {
           if ( exists $h->{Copied} ) {
              $h->{Created} = $h->{Copied};
           }
        }
        # "Last Update" may now appear as "Never" !
        # "Backup" too, but we do not process that field (yet)
        if ( exists $h->{Updated} && $h->{Updated} eq 'Never' ) {
           $h->{Updated} = $h->{Created};
        }
        # add VolID and Voltype for listvldb entries
        if ( exists $h->{Name} and ! exists $h->{VolID} ) {
           (my $volname = $h->{Name}) =~ s/\.readonly$|\.backup$//;
           if ( exists $h->{RWVol} ) {
               $h->{VolID} = $h->{RWVol};
               $h->{Voltype} = 'RW';
           } elsif ( exists $h->{ROVol} ) {
               $h->{VolID} = $h->{ROVol};
               $h->{Voltype} = 'RO';
               $volname .= '.readonly';
           } elsif ( exists $h->{BKVol} ) {
               $h->{VolID} = $h->{BKVol};
               $h->{Voltype} = 'BK';
               $volname .= '.backup';
           } else {
               _print 'W', "cannot find ID/Type for volume '".$h->{Name}."'";
           }
           $h->{Name} = $volname;
        }
        $h->{Locked} = 1 if exists $h->{Locked} and $h->{Locked} eq 'LOCKED';
    }
}

sub vos_parse {
    # parse (vos) output according to $rules
    my ($input, $rules) = @_;
    my $parsetime = time();
    my @results = ();
    my $res = {};
    my $oldstate = 0;
    my $line = 0;
    # parse each input line in turn
    for ( split "\n", $input ) {
        $line++;
        my $matchseen = 0;
        for my $rule ( @$rules ) {
            my ($match, $state, @vars) = @$rule;
            # apply rule unconditionally if state = 0 or state increasing
            next if $state and $oldstate > $state;
            # do not try rules for different state, if match already seen
            next if $matchseen and $state and $oldstate != $state;
            my @matched = /$match/;
            if (@matched) {
                $matchseen = 1;
                # rule that does not set variables
                next if ! defined $vars[0];
                # fill array if variable name starts with @
                (my $firstkey = $vars[0]) =~ s/^@//;
                $oldstate = $state;
                # instead of overwriting vars, store results and fill a new hash
                if (exists $res->{$vars[0]}) {
                    _print 'W', "new result when state = $oldstate" if $state;
                    $res->{_time} = $parsetime;
                    push @results, $res;
                    $res = {};
                }
                if ( $vars[0] =~ /^@/ ) {
                    push @{$res->{$firstkey}}, [ @matched ];
                } else {
                    for my $var ( @matched ) {
                        my $key = shift @vars;
                        $res->{$key} = $var if defined $var;
                    }
                }
            }
        }
        _print 'W', "unparseable ($line):$_" if ! $matchseen;
    }
    if (keys %$res) {
        $res->{_time} = $parsetime;
        push @results, $res;
        return \@results;
    }
    return;
}

sub vos_examine {
#
# parse the output of vos examine and return the information for all volumes
# in the hash %Vols. Both the volume name and volume ID can be used as keys.
#
# arguments   : volid or volname
#
  local $_; # do not clobber the global $_
  if ( $#_ == 0 ) {
    my $parms = $USE_EXT ? "-noauth -ext" : "-noauth";
    $_ = vos_execute("examine @_ $parms");
  } else {
    _print 'D', "wrong number of args (", $#_+1, "), expected 1";
  }
  return if /^VLDB: no such entry/;
  return if /^Could not fetch the information/;
  s/^Volume .* does not exist in VLDB\n+//;
  s/^Dump only information from VLDB\n+//;

  my $res = vos_parse($_, $vos_examine);
  if ( $res ) {
    vos_normalize($res);
    # we need to merge results if more than one hash (RO volumes)
    if ($#$res) {
      my $resref = shift @$res;
      while (my $hash = shift @$res) {
        next unless exists $hash->{Accesses};
        if (exists $hash->{Name} and exists $resref->{Name}
            and $hash->{Name} ne $resref->{Name} ) {
            _print 'W', "got unexpected result for '$resref->{Name}' when merging 'vos exa' for '$hash->{Name}': $_";
        } elsif (exists $hash->{VolID} and exists $resref->{VolID}
             and $hash->{VolID} ne $resref->{VolID} ) {
            _print 'W', "got unexpected result for '$resref->{VolID}' when merging 'vos exa' for '$hash->{VolID}': $_";
	} else {
	    for my $key ( keys %{$hash->{Accesses}} ) {
		$resref->{Accesses}->{$key} = $hash->{Accesses}->{$key};
	    }
        }
      }
      $res->[0] = $resref;
    }
  } else {
    return;
  }
  # for vos examine we do expect one output hash only
  my $voldesc = $res->[0];
  my $id = $voldesc->{VolID};
  $voldesc = voldesc2vols($id, $voldesc);
  my $sites = $voldesc->{Sites} || 0;
  if ( $sites > 1 ) {
    my $vol2 = generate_voldata($voldesc);
    return ($voldesc, $vol2) if $vol2;
  }
  return ($voldesc);
}

sub generate_voldata {
  my $vol = $_[0];
  return unless $vol->{Sites} or $vol->{Sites} > 1;
  return unless $vol->{Name};
  return unless $vol->{VolID};
  my ($vol2);
  if ( $vol->{VolID} eq $vol->{RWVol} ) {
    # we have an RW volume, make the RO volume
    my $roid = $vol->{ROVol};
    if ( defined $roid ) {
      my $name = $vol->{Name}.'.readonly';
      if ( exists $Vols{$roid} ) {
        $vol2 = $Vols{$roid};
      } else {
        $vol2 = {};
        $Vols{$roid} = $vol2;
      }
      $Vols{$name} = $vol2;
      $vol2->{Name} = $name;
      $vol2->{VolID} = $roid;
      $vol2->{Voltype} = 'RO';
      $vol2->{Sites} = $vol->{Sites};
      $vol2->{RWVol} = $vol->{RWVol};
      $vol2->{ROVol} = $roid;
      $vol2->{BKVol} = $vol->{BKVol} if exists $vol->{BKVol};
      @{$vol2->{Partition}} = @{$vol->{Partition}};
      $vol2->{_time}     = $vol->{_time};
    } else {
      _print 'W', "$vol->{Name} with $vol->{Sites} sites not released yet" if $opt_v;
      return undef;
    }
  } elsif ( $vol->{VolID} eq $vol->{ROVol} ) {
    # we have an RO volume, make the RW volume
    my $rwid = $vol->{RWVol};
    (my $name = $vol->{Name}) =~ s/\.readonly$//;
    if ( exists $Vols{$rwid} ) {
      $vol2 = $Vols{$rwid};
    } else {
      $vol2 = {};
      $Vols{$rwid} = $vol2;
    }
    $Vols{$name} = $vol2;
    $vol2->{Name} = $name;
    $vol2->{VolID} = $rwid;
    $vol2->{Voltype} = 'RW';
    $vol2->{Sites} = $vol->{Sites};
    $vol2->{RWVol} = $rwid;
    $vol2->{ROVol} = $vol->{ROVol};
    $vol2->{BKVol} = $vol->{BKVol} if exists $vol->{BKVol};
    @{$vol2->{Partition}} = @{$vol->{Partition}};
    $vol2->{_time}     = $vol->{_time};
  } elsif ( $vol->{VolID} eq $vol->{BKVol} ) {
    # we have an BK volume, make the RW and RO volumes
    ### to be done ###
  } elsif (!exists($vol->{BKVol})) {
    $vol2->{Name} = $vol->{Name}.'.backup';
    $vol2->{Voltype} = 'BK';
    $vol2->{RWVol} = $vol->{RWVol};
    @{$vol2->{Partition}} = @{$vol->{Partition}};
  } else {
     # we should never get here
     Vos::verbose(1); # for full backtrace
     no warnings 'uninitialized';
    _print 'D', "Boum! Vos.pm inconsistency in Volume information ($vol->{Name} / $vol->{VolID} / $vol->{RWVol} / $vol->{ROVol} / $vol->{BKVol})";
  }
  return $vol2;
}

sub vos_listvol {
#
# parse the output of vos listvol -long
#
# arguments   : server [partition]
# return value: array of volume descriptions (representation: anonymous hash)
#
  my ($voldesc, $res);
  # do allow also the server/partition notation (one argument only)
  (local $_="@_") =~ s/\// /;
  my @args = split /\s+/, $_;
  if ( $#args == 0 or $#args == 1 ) {
    my $parms = $USE_EXT ? "-long -noauth -ext" : "-long -noauth";
    # New: vos listvol may return a lot of "**** Volume nnnnnnnnn is busy ****" messages
    # experience shows that in the rare cases this happens, an subsequent vos listvol usually works
    my $i = 3;            # we'll not issue more than 3 vos listvol
    while($i--) {      
      $res = vos_execute("listvol @args $parms");
      return badargs($res,"listvol @args")
        if $res =~ /does not exist on the server/;
      return badargs($res,"listvol @args")
        if $res =~ /Could not fetch the list of partitions from the server/;
      my $k = ($_ = $res) =~ s/is busy \*\*\*\*//go;      # count the "is busy" strings
      last if $k < 50;                           # else retry (once)
      sleep(1);
    }
  } else {
    _print 'D', "wrong number of args (", $#args+1, "), expected 1 or 2";
  }
  # force a null value to $part even when only server specified
  my ($srv, $part) = (@args, "");
  $srv =~ s/\..*$//;
  $Part{$srv} = []  if $#args == 0;
  my $href = vos_parse($res, $vos_listvol);
  if ( $href ) {
    vos_normalize($href);
  } else {
    return;
  }
  my @keys;
  for my $voldesc ( @$href ) {
    my $id = $voldesc->{VolID};
    # retrieve server/part info early, not each entry may have it
    @keys = keys %{$voldesc->{Accesses}} if exists $voldesc->{Accesses};
    # Results from last line of output for a partition, store some attributes
    if ( exists $voldesc->{Online} ) {
      if ( $#keys == 0 ) {
        my $key = $keys[0];
        my ($srv, $part) = split('/', $key);
        push @{$Part{$srv}}, $key if $#args == 0;
        if ( ! exists $Part{$key} ) {
          $Part{$key} = {};
        }
        $Part{$key}->{Partition} = $key;
        $Part{$key}->{onLine} = $voldesc->{Online};
        $Part{$key}->{offLine} = $voldesc->{Offline};
        $Part{$key}->{busy} = $voldesc->{Busy};
        $Part{$key}->{Volumes} = $voldesc->{Online} +
                                 $voldesc->{Offline} + $voldesc->{Busy};
        $Part{$key}->{_time} = $voldesc->{_time};
      }
      delete $voldesc->{Online};
      delete $voldesc->{Offline};
      delete $voldesc->{Busy};
    }
    voldesc2vols($id, $voldesc) if $voldesc->{Name};
  }
  pop @$href if ! exists $href->[-1]->{Name};
  calc_committed ("$srv/$part", @$href) if $part;
  return @$href;
}

sub voldesc2vols {
    my ($id, $voldesc) = @_;
    if ( exists $Vols{$id} ) {
        for my $key ( keys %$voldesc ) {
            if ( $key eq 'Accesses' ) {
                for my $k (keys %{$voldesc->{Accesses}}) {
                    $Vols{$id}->{Accesses}->{$k} = $voldesc->{Accesses}->{$k};
                }
            } else {
                $Vols{$id}->{$key} = $voldesc->{$key};
            }
        }
        $voldesc = $Vols{$id};
    } else {
        $Vols{$id} = $voldesc;
    }
    $Vols{$voldesc->{Name}} = $voldesc if exists $voldesc->{Name};
    return $Vols{$id};
}

sub calc_committed {
  my $comm = 0;
  my %seen;
  my $part = shift;
  for ( @_ ) {
# do not count backup and readonly volumes on the same partition
    (my $vol = $_->{Name}) =~ s/\.readonly$|\.backup$//;
    $comm += $_->{Quota} unless $seen{$vol};
    $seen{$vol}++;
  }
  $Part{$part}->{Committed} = 0;
  new_committed($part,$comm);                 # which may update the disk cache
}

sub vos_listvldb {
#
# parse the output of vos listvldb
#
# arguments(1): volid or volname
#          (2): server [ and partition ]
#               partition may be '' in which case all partitions are listed
# return value: array of volume descriptions (representation: anonymous hash)
#               if a volume is not existing, a hash entry containing undef is
#               created anyway
#
  my ($voldesc, @voldesc, $vid, $vn, $res);
# allow for compact notation 'server/part'
  (local $_="@_") =~ s/\// /;
  my @args = split /\s+/, $_;
  if ( $#args == 0 ) {    # one argument only: volname, or volid, or complete server
    my @servers = get_fsservers();
    my $srv = $args[0];
    foreach (@servers) {
      if ($_ =~ /^$srv(\..*)?$/) { push @args, "" ; last; }
    }
  }
  if ( $#args == -1 ) {        # no arguments at all
    $res = vos_execute("listvldb -noauth -quiet");
    return undef if $?;
  } elsif ( $#args == 0 ) {
# only return an already existing structure if vldb specific data filled in
    if (exists $Vols{$args[0]} and exists $Vols{$args[0]}->{Sites}) {
      return $Vols{$args[0]};
    }
    $res = vos_execute("listvldb @args -noauth");
    return $Vols{$args[0]} = undef if $?;
    $vid = $args[0] if $args[0] =~ /^\d+$/;
    $vn = $args[0] if $args[0] !~ /^\d+$/;
  } elsif ( $#args == 1 and ($args[1]) ) {
    $res = vos_execute("listvldb -server $args[0] -partition $args[1] -nosort -noauth -quiet");
    return undef if $?;
  } elsif ( $#args == 1 ) {
    $res = vos_execute("listvldb -server $args[0] -nosort -noauth -quiet");
    return undef if $?;
  } else {                     # too many
    _print 'D', "wrong number of args (", $#args+1, "), expected 0 to 2";
  }
  # iterate through the volume descriptions
  my $href = vos_parse($res, $vos_listvldb);
  if ( $href ) {
    vos_normalize($href);
  } else {
    return;
  }
  # adjust hash, if a single volume was queried by name or id
  if ( $vid or $vn ) {
    _print 'W', "$#$href additional result(s)" if $#$href;
    my $voldesc = $href->[0];
    my $id = $voldesc->{VolID} if exists $voldesc->{VolID};
    my $vol = $voldesc->{Name} if exists $voldesc->{Name};
    my $vtp = $voldesc->{Voltype} if exists $voldesc->{Voltype};
    my $roid = $voldesc->{ROVol} if exists $voldesc->{ROVol};
    my $bkid = $voldesc->{BKVol} if exists $voldesc->{BKVol};
    if ( $vid ) {
      $id = $vid;
      if ( defined $roid and $id eq $roid and $vol !~ /\.readonly$/ ) {
         $vol .= '.readonly';
         $vtp = "RO";
      }
      if ( defined $bkid and $id eq $bkid and $vol !~ /\.backup$/ ) {
         $vol .= '.backup';
         $vtp = "BK";
      }
    } elsif ( $vn ) {
      $vol = $vn;
      if ( $vol =~ /\.readonly$/ ) {
         $id = $roid;
         $vtp = "RO";
      }
      if ( $vol =~ /\.backup$/ ) {
         $id = $bkid;
         $vtp = "BK";
      }
    }
    $voldesc->{VolID} = $id;
    $voldesc->{Name} = $vol;
    $voldesc->{Voltype} = $vtp;
  }
  for my $voldesc ( @$href ) {
    my $id = $voldesc->{VolID};
    next unless (defined $id && $id ne ''); # 'vos listvldb' can have entires without ID?
    $voldesc = voldesc2vols($id, $voldesc);
    push @voldesc, $voldesc;
    my $sites = $voldesc->{Sites} || 0;
    if ( $sites > 1 ) {
      my $vol2 = generate_voldata($voldesc);
      push @voldesc, $vol2 if $vol2;
    }
    undef $voldesc;
  }
  return @voldesc;
}

#################################################################################
# fullpath - find executable in PATH
#
sub fullpath {
  my ($prog, @path) = @_;
  @path = ( $ENV{PATH} ) if $#_ == 0;
  for my $path (@path) {
    for (split /:/, $path) {
      return "$_/$prog" if -x "$_/$prog" and ! -d "$_/$prog";
    }
  }
  _print 'E', "$prog not found in @path";
  return undef;
}

sub vos_execute {
  my ($vosargs, $noaction, $verbosity) = @_;
  $verbosity = 0 if ! defined $verbosity;
  my $cmd = "$vos $vosargs";
  _print $noaction ? 'would ' : '', "execute $cmd 2>&1" 
         if $noaction or ($opt_v and $opt_v>1) or ($verbosity>1);
  my $res = "";
  if ( $noaction ) {
    return ($verbosity >= 0) ? 0 : "";
  } else {
    if ($verbosity) {
      system $cmd;
      $res = $?;
    } else {
      $res = `$cmd 2>&1`;
    }
  }
  # This often results in the error "No child processes"
  $? = 0 if $? == -1 and $! =~ /No child processes/;
  my $signal = $? & 127 ? " Signal ".($? & 127) : '';
  $signal .= 'Dumped core' if $? & 128;
  _print 'E', "$cmd\n\tERROR(", $? >> 8, ")$signal: $res" if $? and $opt_v;
  return $res;
}


1;
__END__

=head1 NAME

Vos - Perl interface to vos examine, vos listvol, vos listvldb and vos partinfo

=head1 SYNOPSIS

  use Vos;

     information retrieval (file servers and partitions)

  @servers = get_fsservers();
  @partitions = partitions($server);

  $attribute = get_partattrib($partition, $attr_name);

  # shortcuts for the get_partattrib calls
  $free_percent = free_percent($partition);     # 'PercentFree' attribute
  $usedblocks = disk_used($partition);          # 'Used' attribute
  $partsize = disk_capacity($partition);        # 'Total' attribute
  $committed = committed($partition);           # sum of 'quotas' on that
                                                # partition

  # usually vos_partinfo is called from the functions above,
  # no need to call it directly
  @partdesc = vos_partinfo($server);
  @partdesc = vos_partinfo($partition);

     information retrieval (volumes)

  @volumes = volumes();
  @volumes = volumes($server);
  @volumes = volumes($partition);
  exa_single_volumes($value);
  exa_use_ext($value);

  $attribute = get_volattrib($vol_name_or_ID, $attr_name);

  # shortcuts for a few get_volattrib calls
  $volname = vid2name($vol_name_or_ID);         # 'Name' attribute
  $volID = volid($vol_name_or_ID);              # 'VolID' attribute
  @partition = afsvol_part($vol_name_or_ID);    # 'Partition' attribute
  $partition = afsvol_part($vol_name_or_ID, $server);   # as above
  $rwsite = rw_site($vol_name_or_ID);           # RW site of volume
  $quota = quota($vol_name_or_ID);              # 'Quota' attribute
  $blocks = blocks($vol_name_or_ID);            # 'Blocks' attribute
  %accesses = accesses($vol_name_or_ID);        # 'Accesses' attribute
  $accesses = accesses($vol_name_or_ID, $partition);    # as above
  $last_update = last_update($vol_name_or_ID);  # 'Updated' attribute

  # usually vos_listvldb, vos_listvol examine and vos_examine are called
  # from the functions above, no need to call it directly
  @voldesc = vos_listvldb($vol_name_or_ID);
  @voldesc = vos_listvldb();
  @voldesc = vos_listvldb($server, $partition_letters);
  @voldesc = examine($vol_name_or_ID);
  @voldesc = vos_listvol($partition);
  @voldesc = vos_examine($vol_name_or_ID);

     refreshing and purging of information

  set_cache_time($seconds);
  purge_diskinfo($partition);
  purge_volinfo($vol_name_or_ID);
  new_committed($partition, $increment);

     information retrieval (auxiliary functions)

  Vos::verbose($value);
  $fullpath = fullpath($command, @path); 
  noaction($value);
  $rc = vos_execute($subcommand, $args);

=head1 DESCRIPTION

The routines in Vos.pm are ment to enhance the functionality of the AFS
module which is currently lacking support for the vos suite of commands.
At the moment only the routines, that return volume and partition data
are implemented. Whereever a partition is mentioned it is written as
"srv/part" where part is the partition name without the leading "vicep".

=head2 information retrieval (file servers and partitions)

=over 4

=item C<get_fsservers>

         @servers = get_fsservers();

get a list of all fileservers known to AFS. This function is using a
vos listaddrs call which is not implemented in older AFS versions.
If in doubt check vos help before using this function.

=item C<partitions>

         @partitions = partitions($server);

report all the partitions on a given fileserver.

=item C<get_partattrib>

  $attribute = get_partattrib($partition, $attr_name);

get_partattrib can be used to extract individual attributes from the (internal)
partition attributes hash. The following attributes are defined:

 Attribute  reported by partinfo listvol data returned ($ = scalar)
 ------------------------------------------------------------------
 Partition                 *        *    $ (server/partition)
 PercentFree               *             $ (0..100)
 Used                      *             $ (in blocks of 1024k)
 Total                     *             $ (in blocks of 1024k)
 Committed                          *    $ (sum of volume quotas)
 onLine                             *    $ (number of volumes)
 offLine                            *    $ (number of volumes)
 busy                               *    $ (number of volumes)
 Volumes                            *    $ (number of volumes)
 _time                                   $ last update of attributes

=item shortcuts for the C<get_partattrib> calls

  $free_percent = free_percent($partition); # 'PercentFree' attribute
  $usedblocks = disk_used($partition);      # 'Used' attribute
  $partsize = disk_capacity($partition);    # 'Total' attribute

=item C<vos_partinfo>

  @partdesc = vos_partinfo($server);
  @partdesc = vos_partinfo($partition);

Usually vos_partinfo is called from the functions above, it needs not to be
called directly. It returns an array of pointers to partinfo hashes that can
be accessed as outlined in the example below:

  for $phash (@partdesc) {
    $partname   = $phash->{Partition};
    $usedblocks = $phash->{Used};
  }

=back

=head2 information retrieval (volumes)

=over 4

=item C<volumes>

         @volumes = volumes();
         @volumes = volumes($server);
         @volumes = volumes($partition);

The volumes call without parameters is retrieving all volume B<names> reported
by VLDB, i.e. it is calling vos listvldb. In contrast all other volume calls do
report the volume B<IDs> on a given fileserver or on a specific partition.
In the latter case the vos listvol call is used which does not consult 
the VLDB. This function can be very useful to loop over volumes found on a
given server or partition or to detect discrepancies between the VLDB and
the volumes stored on the file servers.

=item C<exa_single_volumes>

         exa_single_volumes($value);

If called with nonzero argument, subsequent calls to retrieve attributes from
volumes will result in a vos examine call (if attributes not cached already),
otherwise the attributes of all volumes in the same partition will be retrieved
at the same time using a vos listvol call. Default is to use vos listvol for
the whole partition, which might be a fairly large overhead. On the other hand
it will speed up all other lookups from the same partition.

=item C<exa_use_ext>

         exa_use_ext($value);

If called with nonzero argument, subsequent calls to retrieve attributes from
volumes will use the -ext option, making available the number of files per
volume.

=item C<get_volattrib>

  $attribute = get_volattrib($vol_name_or_ID, $attr_name);

get_volattrib can be used to extract individual attributes from the
volume attributes hash. The following attributes are defined:

 Attribute from vos exa listvol listvl data returned ($ = scalar)
 ----------------------------------------------------------------
 Name                *      *       *   $
 VolID               *      *       *   $
 Partition           *              *   $->@ (server/partition)
 Voltype             *      *       *   $    (RW, RO, BK)
 Blocks              *      *           $    (in blocks of 1024k)
 Files               *      *           $    (if exa_use_ext(1) has been called)
 Status              *      *           $    (On-line, ...)
 Quota               *      *           $    (in blocks of 1024k)
 Created             *      *           $    (ASCII date string)
 Updated             *      *           $    (ASCII date string)
 Accesses            *      *           $->% (pointer to a hash)
 Sites               *              *   $
 RWVol               *              *   $    (volume ID)
 ROVol (if existing) *              *   $    (volume ID)
 BKVol (if existing) *              *   $    (volume ID)
 ReleaseError        *              *   $->@ (pointer to an array)
 Locked (if locked)  *              *   $    (contains 1 if locked)
 _time                                  $    last attributes update

The ReleaseError attribute contains 'Not released' if there are RO copies
defined but the volume was never released or 'Old release' and 'New release'
if the release of the volume failed. The ReleaseError array has the same size
and ordering as the Partition array. The contents of the Accesses attribute
field is described in the accesses function. It always contains the numbers
for one type of volume only (RW or RO or BK).

=item shortcuts for a few C<get_volattrib> calls

  $volname = vid2name($vol_name_or_ID);         # 'Name' attribute
  $volID = volid($vol_name_or_ID);              # 'VolID' attribute
  @partition = afsvol_part($vol_name_or_ID);    # 'Partition' attrib

the first element in the return array always refers to the RW volume

  $partition = afsvol_part($vol_name_or_ID, $server);   # as above

if RW and RO volumes are on different partitions, then the partition refers to
the RW volume. In order to get the name of the RW site the call 

  $rwsite = rw_site($vol_name_or_ID);           # RW site of volume

is faster than calling afsvol_part and looking at element zero of the result.

  $quota = quota($vol_name_or_ID);              # 'Quota' attribute
  $blocks = blocks($vol_name_or_ID);            # 'Blocks' attribute
  %accesses = accesses($vol_name_or_ID);        # 'Accesses' attrib

The keys of the %accesses hash are the partition names (e.g. afssrv1/a ).
If the argument refers to the RW volume, only accesses to the RW volume
are returned. If the RO name or ID is given, all accesses to the RO volumes
are stored in the %accesses hash. If the BK name or ID is given, the accesses
to the BK volume are returned.

  $accesses = accesses($vol_name_or_ID, $partition);    # as above

Here again it is important to refer either to the RO or RW (or even BK) volume
to get the correct values.

  $last_update = last_update($vol_name_or_ID);  # 'Updated' attrib

=item C<vos_listvldb>

  @voldesc = vos_listvldb($vol_name_or_ID);
  @voldesc = vos_listvldb();
  @voldesc = vos_listvldb($server, $partition);

read in the whole VLDB database or parts of it. Subsequent calls to retrieve
the appropriate volume attributes will then make use of the stored values
unless cache_time is exceeded. Usually vos_listvldb is called from the
functions above, there is no need to call it directly. The function returns
an array of pointers to volinfo hashes that can be accessed as outlined in the
example below:

  for $vhash (@voldesc) {
    $volname = $vhash->{Name};
    $voltype = $vhash->{Voltype};
  }

Please note that vos_listvldb will not fill all possible attributes (see the
table above)

=item C<examine>

  @voldesc = examine($vol_name_or_ID);

This function will normally call vos_listvol for the whole partition that does
contain the volume and returns all the volume descriptions found there. If
exa_single_volumes(1) was called, then vos_examine will be called instead.
That returns volinfo hashes for the current volume only (could be more than
one)

=item C<vos_listvol>

  @voldesc = vos_listvol($partition);

Returns the volinfo hashes for all volumes found on that partition. Please note\that vos_listvol  will not fill all possible attributes.

=item C<vos_examine>

  @voldesc = vos_examine($vol_name_or_ID);

Returns volinfo hashes for the current volume (could be more than one)

=back

=head2 refreshing and purging of information

=over 4

=item C<set_cache_time>

  set_cache_time($seconds);

the cached attribute values will be regarded valid for the given time.
Default is 24*3600 seconds (one day).

=item C<purge_diskinfo>

         purge_diskinfo($partition);

Deletes all cached information on a given partition.

=item C<purge_volinfo>

         purge_volinfo($vol_name_or_ID);

Deletes all cached information on a given volume.

=item C<new_committed>

         new_committed($partition, $increment);

Updates the "Committed" attribute from the partition information, if the
attribute is already present.

=back

=head2 misc functions

=over 4

=item C<verbose>

         Vos::verbose($value);

set the verbose option to value. Presently only 0 and 1 are supported, default
is 0.

=item C<fullpath>

         $fullpath = fullpath($command, @path);

lookup the executable $command in directories given in @path, where @path is
a list of strings containing colon delimited directories like the contents of
$ENV{PATH}. If @path is not given, it defaults to $ENV{PATH}.

=item C<noaction>

         noaction($value)

Influences the behaviour of the vos_execute function. If called with a nonzero
argument, vos execute will not call vos but only print the command.

=item C<vos_execute>

         $rc = vos_execute($subcommand, $args);

do a vos subcommand args and return the error code. When the noaction routine
was called before it only prints what would be executed.

=back

=head1 AUTHORS

Wolfgang Friebel C<Wolfgang.Friebel@desy.de>
Bernard Antoine C<Bernard.Antoine@cern.ch>

=head1 SEE ALSO

AFS module (perldoc AFS)

=cut
