# Volset.pm - routines to manipulate a set of AFS volumes (characterized by
#             a common name pattern, used to be called projects or by the
#             partitions they live in, used to be called pools). The AFS volume
#             sets are described in a configuration file afsadmin.cf
#
#
# there are a few CERN specific things left
# (marked by ########## CERN specific ########## lines)
#
# Wolfgang Friebel 11/2001 (last update Sep 17, 2002)
# Bernard Antoine 02/2004 to add CacheCommitted
#                 03/2004 to modify part_count
#                 07/2005 to move major parts to server side
#                 05/2006 fixes to adapt to Desy version of Vos
#
package Volset;

use strict;
use Exporter;
use Vos;
use Cwd;
use lib "/afs/cern.ch/project/afs/perlmodules"; # needed for ConsoleDBconf.pm
use vars qw (@ISA @EXPORT $VERSION $CFLOC $KRBLOC $PGMLOC $COMMDIR $extra_pts_pfx
             %Servertypes %Partitions %Partopts $opt_f $opt_v
             $want_arc $must_arc %DrainPartitions @Volsetpattern @Volsetnames %Volsetquota
             %Volsetmnt %Volsetopts $Init_done $AFSCell $AFSRWCell $AFSHome
             $arc $arcserver $mynode
             $strategies %Str_fact
             $Sec_group @Sec_wide @Sec_filter
         );
@ISA = qw(Exporter);
@EXPORT = qw ( 
               vol_create vol_delete vol_create_replica vol_release vol_rename vol_quota
               is_partition is_volset homedir whoami volset_quota decode_quota encode_quota
               volmove choose_disk volsetopts     move_2G_volumes use_arc
               IOpenalty validate_acl
               vol2set parentsets get_afsservers get_serverfeatures get_partfeature
               pools toplevel_pools get_pooldisks check_mount check_volume
               get_volsets set2pat drain_partition get_obsolete_partitions get_readonly_partitions
		getpvbyvol
          );
$VERSION = 1.13;

package Vol_Do;
use File::Basename;
use vars qw (
               $PVDIR $QVDIR $TMPDIR $arccaller
            );
$arccaller = "";

########## CERN specific ##########
package Volset;
$KRBLOC = "/usr/kerberos/bin";
$PGMLOC = "/usr/bin:/usr/sbin:/usr/sue/bin";
$CFLOC  = "/afs/cern.ch/project/afs/var/adm/afsadmin.cf";
$COMMDIR= "/afs/cern.ch/project/afs/var/comm_cache";     # cache for partitions quotas == commit
$extra_pts_pfx = "cern:";                                # to locate group gg through pts exam cern:gg
package Vol_Do;
$PVDIR  = "/var/afs_admin/pv";                    # cache for volume quota
$QVDIR  = "/var/afs_admin/qv";                    # cache for volume accesses
$TMPDIR = "/afs/cern.ch/project/afs/var/tmp";     # tmp dir for 'fs sq'
########## CERN specific end ##########

package Volset;

$want_arc = 0;                                       # until one calls use_arc
$must_arc |= ($> != 0);                              # most routines in Vol_Do need to run as root
$arcserver = '';

# defer final initialization until after compilation
INIT {
  $arcserver = (get_afsservers('arc'))[0] || 'afsarc' unless $arcserver;
  unless ($mynode) {
    $mynode = `hostname`;  chomp $mynode;
    $mynode =~ s/\..*$//;
  }
  $must_arc |= ($mynode ne $arcserver);              # .. and need to run on the #arcserver node
}

sub read_cf {
  return if defined $Init_done;
  local $_;
  open CF, $CFLOC or &_print('D', "Could not open $CFLOC:$!");
  my $section = '';
  %Volsetopts = ( backup => {},
		  committed => {},
		  newquota => {},
		  maxquota => {},
		  newios   => {},
		  orphanedvols => {},
		  space => {},
		  overfull => {},
		);
  while (<CF>) {
    next if /^#/;
    next if /^\s*$/;
    chomp;
# a section line [SECTIONNAME]
    if ( /^\s*\[(.*)\]\s*$/ ) {
      $section = $1;
# section AFSSERVERS
    } elsif ( $section eq 'AFSSERVERS') {
      my ($host, $os, $vers, $types) = split ' ', $_, 4;
# unqualified hostname only
      $host =~ s/\..*$//;
#      $Opsys{$host} = $os;
#      $AFSVers{$host} = $vers;
      $Servertypes{$host} = [ $os , split(/\s+/, $types)];
# section PARTITIONS
    } elsif ( $section eq 'PARTITIONS') {
      my ($part, $sets) = split ' ', $_, 2;
      $sets = '' unless defined $sets; # the sets field can be empty
      my @sets;
      my %features;
      foreach (split ' ', $sets) {
        if (/([^=]+)=(.+)/) {
          $features{$1} = $2;
          $Partopts{$1} = 1;    # remember names
        } else {
          push @sets, $_;
        }
      }
      my @parts = ();
      if ( $part =~ /^(.*)\/\((.*)\)$/ ) {
        my $srv = $1;
        my @p = split /,/, $2;
        for ( @p ) {
          push @parts, "$srv/$_";
        }
      } else {
        push @parts, $part;
      }

      foreach (@parts) {
        $Partitions{$_}->{Sets} = [ @sets ];
        foreach my $f (keys %features) {
          $Partitions{$_}->{$f} = $features{$f};
        }
      }
# section VOLUMEPATTERNS
    } elsif ( $section eq 'VOLUMEPATTERNS') {
      my ($pat,$rep) = split ' ', $_, 2;
    no strict 'refs';               # allow variables
      $pat =~ s/\$([a-zA-Z_]+)/${$1}/g;     # in the pattern
    use strict 'refs';
#      push @Volsetpattern, qr /$pat/;
      push @Volsetpattern, $pat;
      push @Volsetnames, $rep;
# section AFSVOLSET
    } elsif ( $section eq 'AFSVOLSET') {
      my @items = split;
      my ($set,$quota,$mnt) = grep { $_ !~ /=/ } @items;
      my @flags = grep { /=/ } @items;
      if ( defined $set ) {
      no strict 'refs';                                # allow variables
        $mnt =~ s/\$([a-zA-Z_]+)/${$1}/g if defined $mnt;   # in the mount pattern
      use strict 'refs';
        $Volsetquota{$set} = $quota;
        $Volsetmnt{$set} = $mnt;
      } else {
# handle lines that contain flags only (goes to Defaults)
        $set = 'Defaults';
      }
      for my $flag ( @flags ) {
        $Volsetopts{$1}->{$set} = $2 if $flag =~ /([^=]+)=(.*)/;
      }
      for (keys %Volsetopts) {
        next if defined $Volsetopts{$_}->{$set};
        next unless defined $Volsetopts{$_}->{Defaults};
        $Volsetopts{$_}->{$set} = $Volsetopts{$_}->{Defaults};
      }
# section DRAIN
    } elsif ( $section eq 'DRAIN') {
      my @field = split ' ', $_, 2;
      $DrainPartitions{$field[0]} = $field[1] || 0;
# section AFSCELL
    } elsif ( $section eq 'AFSCELL') {
      ($AFSCell, $AFSHome) = split;
      $AFSCell =~ s/^\s//g;
      $AFSCell =~ s/\s$//g;
      $AFSCell =~ s|/\.|/|;
      ($AFSRWCell = $AFSCell) =~ s|/|/.|;
# section STRATEGY
    } elsif ( $section eq 'STRATEGY') {
      my @keys = qw( pu io nbs k attr);
      my ($str, @p) = split;
      $strategies .= $str." ";
      foreach (@keys) {
        $Str_fact{$str}{$_} = 0;
      }
      foreach (@p) {
         &_print('W', "invalid syntax for strategy:$_") unless $_ =~ /([^=]+)=([+-]?\d+)/;
         &_print('W', "invalid syntax for strategy:$_") unless grep {$1 eq $_} @keys;
         $Str_fact{$str}{$1} = $2 if $1;
      }
# section SECURITY
    } elsif ( $section eq 'SECURITY') {
      if    (/^\s*group\s*=\s*([\w\:]+)\s*$/) { $Sec_group = $1; }
      elsif (/^\s*wide\s*=\s*((?:$legal_volname_pattern\:?$legal_volname_pattern\s*)+)$/) { @Sec_wide = split /\s+/, $1; }
      elsif (/^\s*filter\s*=\s*([rlidwka\s]+)\s*$/) { @Sec_filter = split /\s+/, $1; }
      else  { &_print('W', "invalid syntax in security section:$_"); }
# no section
    } elsif ( $section eq '') {
      &_print('D', "Text outside of a section in $CFLOC\n$_");
    } else {
      &_print('D', "Unknown section $section if $CFLOC");
    }
  }
  close CF;
# change DrainPartitions to contain partitions only (no pure host names)
  for my $k ( keys %DrainPartitions ) {
    if ( $k !~ /\// ) {
      my $v = $DrainPartitions{$k};
      delete $DrainPartitions{$k};
      map { $DrainPartitions{$_} = $v } grep { /^$k\// } keys %Partitions;
    }
  }
# add a volset pattern that matches everything (to catch errors)
#  push @Volsetpattern, qr /.*/;
  push @Volsetpattern, '.*';
  push @Volsetnames, 'unknown';
  $Init_done = 1;
}

sub get_afsservers {
  read_cf() unless defined $Init_done;
# an expression containing the characters !, &, | and () is accepted
# interpret multiple arguments as logical AND
# by default, obsolete machines (describe by a  - in afsadmin.cf) are not listed
# a leading '+' in the expression allows listing of all machines
  my $expr = join('&', @_);
  $expr =~ s/\s//g;
  if (substr($expr,0,1) eq '+') { $expr = substr($expr,1); }  # de-disallow obsolete machines
  else {
    if (substr($expr,0,1) eq '-') { $expr = substr($expr,1); }  # disallow obsolete machines
    if ($expr) { $expr = "($expr)&!-"; }
    else       { $expr = "!-"; }
  }
  return sort lexically keys %Servertypes unless $expr;
  _print('E', "Illegal chars in \'$expr\'") if $expr =~ /[^&|()!\-\w]/;
# a bit of woodoo: loop over all hosts (outer grep) and evaluate the expression
# of types for that host (inner grep), accept the host if the expr was true
  $expr =~ s/([^&|()!]+)/(grep {\$_ eq \"$1\"} \@{\$Servertypes{\$_}})/g;
  my @res = sort lexically grep { eval $expr } keys %Servertypes;
  _print('E', "malformed expression: $@\t in '$expr'") if $@;
  return @res;
}

sub get_serverfeatures {
  read_cf() unless defined $Init_done;
  return @{$Servertypes{$_[0]}} if exists $Servertypes{$_[0]};
  return undef unless $_[0] =~ /\//;        # if not a server, try server/part;
  my($s) = $_[0] =~ /(.*)\/.*/;
  return @{$Servertypes{$s}} if exists $Servertypes{$s};
  return undef;
}

sub get_partfeature {
  read_cf() unless defined $Init_done;
  my ($part, $f) = @_;
  if ($f && ! $part) {
    my %res;
    foreach (keys %Partitions) {
       $res{$_}= $Partitions{$_}->{$f} if exists $Partitions{$_}->{$f};
    }
    return %res;
  }
  return undef unless exists $Partitions{$part};
  if ($f) {
    return $Partitions{$part}->{$f} if exists $Partitions{$part}->{$f};
  } else {
    my @res;
    foreach $f (keys %Partopts) {
      push @res, $f . '=' . $Partitions{$part}->{$f} if exists $Partitions{$part}->{$f};
    }
    return "@res" if @res;
  }
  return undef;
}

sub lexically {
  my ($aa,$ab) = ($`,$&) if $a =~ /\d*$/;
  my ($ba,$bb) = ($`,$&) if $b =~ /\d*$/;
  return $aa cmp $ba || ($ab || 0) <=> ($bb || 0);
}

sub whoami {
    my $whoami;
    my $owner = $ENV{'SCPRINCIPAL'};            # if called inside an arc procedure
    if ($owner && $owner !~ /^rcmd\.|^host\//) {        #   but please avoid acrontab!
      ($whoami) = $owner =~ /([^\@]+)\@?.*/;	# includes simply "unknown"!
      $whoami =~ s/\.$//;
    }                                           # else trust the kerberos identity
    unless ($whoami) {
      my $tok = fullpath('klist',$KRBLOC,$PGMLOC);
      my $suid;
      if ($> == 0 and $< != 0) { # running in suid mode?
        $suid = $<;    # keep orig
        $<    = $>;    # become real root
        #_print("totally rooted");
      }
      ($whoami) = `$tok 2>/dev/null` =~ /rincipal:\s+([\w\/\.]+)\@\U$AFSCell\E/;
      $< = $suid if $suid;  # revert
    }
    unless ($whoami) {
      my $tok = fullpath('tokens.krb',$PGMLOC);
      my $suid;
      if ($> == 0 and $< != 0) { # running in suid mode?
        $suid = $<;    # keep orig
        $<    = $>;    # become real root
        #_print("totally rooted");
      }
      ($whoami) = `$tok` =~ /User ($legal_volname_pattern)'s tokens for krbtgt/;   # needs to accept a dot
      $< = $suid if $suid;  # revert
    }
    unless ($whoami) {
      _print('E', "Cannot establish kerberos identity");
      my $id = fullpath("id",$PGMLOC);
      ($whoami) = `$id` =~ /^uid=\d+\(($legal_username_pattern)\)/;
    }
    $whoami =~ s!/!.!;            # if krb5 syntax
    return $whoami;
}

sub homedir {
  my $none = "\x01";
  my $user = shift || $none;
  read_cf() unless defined $Init_done;
  my $home = $AFSHome;
  if ( $home =~ /\$u(\d+)/ ) {
    my $initials = substr($user, 0, $1);
    $home =~ s/\$u\d+/$initials/;
  }
  $home =~ s/\$u/$user/;
  $home =~ s!/$none!!g;
  return $home;
}

sub volsetopts {
  read_cf() unless defined $Init_done;
  my ($name, @set) = @_;
  my $perc = 0;      # no percolation yet
  if ( ! grep { $_ eq $name } keys %Volsetopts ) { 
    &_print('D', "parameter $name not defined for afsadmin.cf");
  } else {
    if ($#set == 0) {
       push @set, parentsets($set[$[]);
    }
    for my $set (@set) {
      if ( exists $Volsetopts{$name}->{$set} ) {
         my $ret = $Volsetopts{$name}->{$set};
         next if $ret eq "";     # item=  equivalent to no item=
         if ( $ret =~ s/^-(.+)/$1/ && $perc ) { next; }
         return $ret;
      }
      $perc = 1;     # now percolating
    }
    return $Volsetopts{$name}->{Defaults};
  }
}

sub check_mount {
  my ($mp, $volset) = @_;
  read_cf() unless defined $Init_done;
  my @set;
#convert relative path into an absolute path
  $$mp = cwd() . '/' . $$mp if $$mp !~ /^\//;
  $$mp = "/afs/$AFSCell/$$mp" if $$mp !~ /^\//;
  $$mp =~ s|/afs/$AFSCell/|/afs/.$AFSCell/|;
# resolve @sys locally before it goes to a server through arc
  if ($$mp =~ /\W\@sys\b/) {
    my $fs = fullpath('fs', $PGMLOC);
    my ($sys) = `$fs sysname` =~ /'(.*)'/;
    $$mp =~ s/(\W)\@sys\b/$1$sys/g;
  }
# remove optional trailing slash
  $$mp =~ s/\/$//;
# allow for mount point with and without dot
  (my $rootpat = '/afs/'.$AFSCell.'/') =~ s|^/afs/|/afs/\\.?|;
  if ( $volset and $Volsetmnt{$volset} ) {
    my $patmatch = $Volsetmnt{$volset};
    if ($$mp =~ /$rootpat$patmatch\b/) {
       _print("mountpoint '$$mp' matches pattern '$patmatch' for set '$volset'") if $opt_v;
       return ($volset);
    }
    return ();
  } elsif ( ! $volset ) {
    for my $p ( keys %Volsetmnt ) {
      next unless $Volsetmnt{$p};
      my $patmatch = $Volsetmnt{$p};
      if ( $$mp =~ /$rootpat$patmatch\b/ ) {
        next unless $p =~ /^__[SU\@]$/ || Vol_Do::check_acl($p);
        _print("mountpoint '$$mp' matches pattern '$patmatch' for set '$p'") if $opt_v;
        push @set, $p;
      }
    }
  }
  return @set if @set;
  _print 'E', "Mountpoint '$$mp' doesnot match any pattern";
  return ();
}

sub check_volume {
  my ($vol, $expect) = (@_, 1);
  unless ($vol =~ /^$legal_volname_pattern$/) {
    _print 'E', "illegal chars in '$vol' (not [A-Za-z_0-9.-]+)";
    return undef;
  }
  my $xx = Vos::verbose(0);
  my @exists = examine($vol);
  Vos::verbose($xx);
  if ($expect) {                 # we want the volume to exist;
     return $vol if @exists;
     _print 'E', "volume '$vol' not found in VLDB";
  } else {
     return $vol unless @exists;
     _print 'E', "volume '$vol' already exists in VLDB";
  }
  return undef;
}

sub get_volsets {
  read_cf() unless defined $Init_done;
  return %Volsetmnt;
}

sub volset_quota {
  read_cf() unless defined $Init_done;
  return $Volsetquota{$_[0]} if exists $Volsetquota{$_[0]};
  return undef;
}

sub encode_quota{
  my ($num, $prec, $roundat)  = (@_, 0 , 0);
  return undef unless $num =~ /([-+]?)(\d+(?:\.\d*)?)([kmgtKMGT][bB]?)?$/;
  my $sign = $1;
  $num = ($3) ? decode_quota("$2$3") : $2;
  $roundat = 29 unless $roundat;
  my ($l);

  if ($num >= 1000000000) {
    $num = $num/1000000000; $l = 4;
  } elsif ($num >= 1000000) {
    $num = $num/1000000;    $l = 3;
  } elsif ($num >= 1000) {
    $num = $num/1000;       $l = 2;
  } elsif ($num >= 1) {
    $num = $num/1;          $l = 1;
  } else {
    $num = $num*1000;       $l = 0;
  }
  if ($num < $roundat ) { $prec++; }
  $num = int($num*10**$prec+.9)/10**$prec;
  $num = $sign . $num . ('', 'k', 'M', 'G', 'T')[$l];
  return $num;
}

sub decode_quota{
  my ($num, $volume) = @_;
  my ($maxquota, @sets);
  if ($volume) { 
     @sets = reverse(vol2set($volume));
     $maxquota = volsetopts('maxquota', @sets);
  }
  return undef unless $num =~ /([-+]?)(\d+(?:\.\d*)?)([kmgtKMGT][bB]?)?$/;
  my $sign = $1;
  $num = $2;
  my $unit = $3;
  if    ( $unit =~ /[Tt]B?$/ )  { $num = $num * 1000000000; }
  elsif ( $unit =~ /[Gg]B?$/ )  { $num = $num * 1000000; }
  elsif ( $unit =~ /[Mm]B?$/ )  { $num = $num * 1000; }
  elsif ( $unit =~ /[Kk]B?$/ )  { $num = $num * 1; }
  else                          { $num = $num * 1; }

  $num = "$sign$num";
  if ($volume) {
    $num = quota($volume) + $num if $sign;
    &_print('D', "This request is really too big.\n\tMaximum quota for pool $sets[0] is $maxquota\n")
      if $num > decode_quota($maxquota);
  }
  return $num;
}

sub is_volset {
  read_cf() unless defined $Init_done;
  return undef unless (defined $_[0]);
  return 1 if exists $Volsetquota{$_[0]};
  foreach (keys %Volsetquota) {
     return 1 if exists $Volsetopts{hint}->{$_} and $_[0] eq $Volsetopts{hint}->{$_};
  }
  return undef;
}

sub parentsets {
  my $set = shift;
  my ($i, %res, @res);
  read_cf() unless defined $Init_done;
########## CERN specific ################################################
# the code below is CERN specific only because the underscore character
# is used in the [VOLUMESETS] section of afs_admin.cf (e.g. $1_q) and
# this same character is hardwired in the code below. This construction
# is used to define projects proj and subprojects proj_p, proj_q etc.
# else, one coudd change all occurences of $setp into $set in the rest 
# of the function
  my $setp = ( $set =~ /^(.*)_\w$/ ) ? $1 : $set;
########## CERN specific end ############################################
  if (exists $Volsetopts{hint}->{$setp} ) { 
######more CERN specific ################################################
# when not using CERN specific, the code below is just
#   $set = $Volsetopts{hint}->{$set};
    my $seth = $Volsetopts{hint}->{$setp};
    $set =~ s/^$setp/$seth/ ;
    $setp = $seth;
    push @res, $setp unless $res{$setp}++;
########## CERN specific end ############################################
    unshift @res, $set unless $res{$set}++;
  }
  for ($i=0; $i<=$#Volsetnames; $i++) {
    my $n = $Volsetnames[$i];
    $n =~ s/\$1/$setp/g;
    if ( $n =~ /(.*)\s+$set\b/ ) {
      for ( reverse split (' ', $1) ) {
        push @res, $_ unless $res{$_}++;
      }
    }
  }
  return @res;
}

sub set2pat {
  my $set = shift;
  my ($i, %rep);
  read_cf() unless defined $Init_done;
  for ($i=0; $i<=$#Volsetnames; $i++) {
    my @pat = split " ", $Volsetnames[$i];
    my $pool = $pat[0];
    my $vsp = $Volsetpattern[$i];
    for my $pat ( @pat ) {
      if ( $pat =~ /^$set$/ ) {
	$rep{$vsp} = $pool;
	last;
########## CERN specific ################################################
      } elsif ( $pat =~ /^\$1_\w$/ and $set =~ /_\w$/ ) {
	(my $setp = $set) =~ s/_[^_]*$//;
	$pat =~ s/\$1/$setp/;
	next if $pat ne $set;
	$vsp =~ s/\(.*\)/$setp/;
	$rep{$vsp} = $pool;
        last;
      } elsif ( $pat =~ /^\$1$/ and $set !~ /_\w$/ ) {
########## CERN specific end ############################################
#     } elsif ( $pat =~ /^\$1$/ ) {
	$vsp =~ s/\([^\)]*\)/$set/;
	$rep{$vsp} = $pool;
      }
    }
    _print("$set uses pattern: $vsp for pool $pool")
	  if $opt_v and exists $rep{$vsp};
  }
  return %rep;
}

sub vol2set {
  my ($i, $a);
  read_cf() unless defined $Init_done;
  local $_;
  $_ = vid2name $_[0];
# treat readonly and backup volumes the same way
  s/\.readonly$|\.backup$//;
  for ($i=0; $i<=$#Volsetpattern; $i++) {
#    last if ($a) = ($_ =~ $Volsetpattern[$i]);
    next unless /$Volsetpattern[$i]/;
    $a = $1;
    $b = $2;
    last;
  }
  my $rep = $Volsetnames[$i];
  $rep =~ s/\$1/$a/g if $a;
  $rep =~ s/\$2/$b/g if $b;
  my @nrep = (split " ", $rep);
  my @rep = ();
  foreach my $set (@nrep) {
     my ($setp, $suff);
     if ( $set =~ /^(.+)(_\w)$/ ) { $setp = $1;   $suff = $2; }
                             else { $setp = $set; $suff = ""; }
     push @rep, $Volsetopts{hint}->{$setp} . $suff if exists $Volsetopts{hint}->{$setp};
     push @rep, $set;
  }
  _print("volume $_[0] is in collections: @rep") if $opt_v>2;
  return @rep;
}

sub pools {
  read_cf() unless defined $Init_done;
  return @{$Partitions{$_[0]}->{Sets}} if exists $Partitions{$_[0]};
  return undef;
}

sub toplevel_pools {
  read_cf() unless defined $Init_done;
  my %pools;
  foreach $a (keys %Partitions) { foreach ( @{$Partitions{$a}->{Sets}} ) { $pools{$_} = 1; }};
# my %pools = map { $Partitions{$_}->[0] || '', 1 } keys %Partitions;
# delete $pools{''};
  return sort keys %pools;
}

sub is_partition {
  read_cf() unless defined $Init_done;
  return defined $Partitions{$_[0]};
}

sub drain_partition {
  read_cf() unless defined $Init_done;
  if ( $_[0] ) {
    return $DrainPartitions{$_[0]} || 1 if defined $DrainPartitions{$_[0]};
    return undef;
  }
  return %DrainPartitions;
}

sub get_obsolete_partitions {
  read_cf() unless defined $Init_done;
  my @obsolete_servers = get_afsservers("+obsolete");  #'+' in order to see '-' disabled machines
  my @obsolete_partitions;
  # do not use partitions(), this would try to contact the server.. which is nearly guaranteed to be down.
  if(@obsolete_servers) {
      my $reobs = join('|',@obsolete_servers);
      @obsolete_partitions = grep { m:^($reobs)/: } sort lexically keys %Partitions;
  }
  return @obsolete_partitions;
}

sub get_readonly_partitions {
  read_cf() unless defined $Init_done;
  my @readonly_servers = get_afsservers("+readonly");  #'+' in order to see '-' disabled machines
  my @readonly_partitions;
  # do not use partitions(), this would try to contact the server.. which is nearly guaranteed to be down.
  if(@readonly_servers) {
      my $reres = join('|',@readonly_servers);
      @readonly_partitions = grep { m:^($reres)/: } sort lexically keys %Partitions;
  }
  return @readonly_partitions;
}

sub get_pooldisks {
  read_cf() unless defined $Init_done;
  return sort lexically keys %Partitions unless @_;
  my %types = map { $_, 1} @_;
  return sort lexically grep {
			       grep { defined $types{$_} } @{$Partitions{$_}->{Sets}}
			     } keys %Partitions;
}

sub validate_acl {
  return () unless @_;
  read_cf() unless defined $Init_done;
  # we receive two lists in one:  current ACLs & new ones.
  # we first convert that into a hash, so newer definitions replace old ones
  # new ACLs with "none" remove old one.
  # also we only keep potentially dangerous groups.
  _print ("D", "invalid acl string specified: '@_'") unless $#_%2;
  my %acl;
  while (my $owner = shift @_) {
     my $acl = shift @_;
      last if $acl eq "rights:";
      if ($acl eq 'read')     { $acl = "rl"; }
      elsif ($acl eq 'write') { $acl = "rlidwk"; }
      elsif ($acl eq 'all')   { $acl = "rlidwka"; }
      elsif ($acl eq 'none')  { $acl = ""; }
      elsif ($acl eq '')      { $acl = ""; }
      elsif ((my @a) = $acl =~ /([^rlidwkaABCDEFGH])/g) { die "invalid characters '@a' in '$acl'\n"; }
     next unless grep {$_ eq $owner} @Sec_wide;
     $acl{$owner} = $acl;
     delete $acl{$owner} if $acl eq "";
  }
  return () unless keys %acl;

  # we now check the resulting list of ACLs
  my $Acl = "";
  my @new = %acl;
  print "resulting acls: @new\n" if $opt_v > 2;
  foreach my $owner (keys %acl) {
     $Acl .= $acl{$owner};        # aggreate potentially dangerous ACLs
  }
  # clean duplicates
  (my $t = "rlidwka") =~ s/[^$Acl]//g;
  $Acl = $t;
  print " aggregated acls = $Acl \n" if $opt_v > 2;

  # from each sec_filter, extract what is legal
  # the length of the filter plus the number of removed characters gives a weight
  # the first filter with the lowest weight is used to create the desired acl
  my $z=99; my $filter="";
  foreach (@Sec_filter) {
     my $a = (my $b = $Acl) =~ s/[^$_]//g;    # what is left
     my $e = $a + length($_);                  # weight
     print "$_ -> $b, $a  -> $e\n" if $opt_v > 2;
     return () unless $a;                     # valid against one filter
     if ($e < $z) { $filter = $b, $z = $e; }   # favor first lowest weight mode
  }
  print "choose filter = $filter\n" if $opt_v > 2;

  @new = ();
  foreach (keys %acl) {
     next unless (my $acl = $acl{$_}) =~ s/[^z$filter]//g; 
     $acl = "none" unless $acl;
     push @new , $_, $acl;
  }
  return @new;
}

sub volmove {
# volmove(volume [, destination]);
  if ($want_arc | $must_arc) {     # if asked to use arc
    return arc_exec_pv(0, "volmove" , @_ );
  } else {
    return Vol_Do::__volmove(@_);
  }
}

sub vol_release {
# vol_release(volume [, what]);
  if ($want_arc            ) {     # no need to use arc
    return arc_exec_pv(0, "vol_release" , @_ );
  } else {
    return Vol_Do::__vol_release(@_);
  }
}

sub vol_create {
# vol_create(volume [, destination [, quota]]);
  if ($want_arc | $must_arc) {     # if asked to use arc
    return arc_exec_pv(0, "vol_create" , @_ );
  } else {
    return Vol_Do::__vol_create(@_);
  }
}

sub vol_delete {
# vol_delete(volume [, destination]);
  if ($want_arc | $must_arc) {     # if asked to use arc
    return arc_exec_pv(0, "vol_delete" , @_ );
  } else {
    return Vol_Do::__vol_delete(@_);
  }
}

sub vol_create_replica {
# vol_create_replica(volume [, destination]);
  if ($want_arc | $must_arc) {     # if asked to use arc
    return arc_exec_pv(0, "vol_create_replica" , @_ );
  } else {
    return Vol_Do::__vol_create_replica(@_);
  }
}

sub vol_rename {
# vol_rename(volume [, new_name]);
  if ($want_arc | $must_arc) {     # if asked to use arc
    return arc_exec_pv(0, "vol_rename" , @_);
  } else {
    return Vol_Do::__vol_rename(@_);
  }
}

sub vol_quota {
# vol_quota(volume [, quota]);
  if ($want_arc | $must_arc) {     # if asked to use arc
    return arc_exec_pv(1, "vol_quota" , @_ );
  } else {
    return Vol_Do::__vol_quota(@_);
  }
}

sub choose_disk {
  if ($want_arc | $must_arc) {
    return arc_exec_pv(1, "choose_disk" , @_ );
  } else {
    return Vol_Do::__choose_disk(@_);
  }
}


sub IOpenalty {
  if ($want_arc | $must_arc) {
    return arc_exec_pv(1, "IOpenalty" , @_ );
  } else {
    return Vol_Do::__IOpenalty(@_);
  }
}


sub move_2G_volumes {
  $opt_f = shift;
}

sub verbose {
  @_ ? ($opt_v = shift) : return $opt_v;
}

sub arc_exec_pv {
  # similar to the arc_execute in afs_admin, but only for "pv",
  # and with optional short timeout

  $arc = fullpath('arc',$PGMLOC) unless $arc;
  my $isfast = shift @_;
  my $function = shift @_;
  my $warn = $^W; $^W=0;
  my (@cmd) = ( $arc , "-h" , $arcserver ,
		(($isfast) ? ("-t", 60, "-T", 600) : ""),
		"pv" ,
		(($opt_v) ? ("-v" , $opt_v) : "") ,
		(&noaction() ? "-n" : "") ,
		($function) ,
		join(',',@_) );
  $^W = $warn;
  _print("execute @cmd") if $opt_v > 1 ;
  my $suid;
  if ($> == 0 and $< != 0) { # running in suid mode?
    $suid = $<;    # keep orig
    $<    = $>;    # become real root
    #_print("totally rooted");
  }
  ## we do not want to expose STDIN. We do not want to start a shell either (</dev/null)
  open (SAVEIN,"<&STDIN");
  open (STDIN, "</dev/null");
  my $res = `@cmd`;
  open (STDIN,"<&SAVEIN");
  close(SAVEIN);
  $< = $suid if $suid;  # revert
  my $signal = $? & 127 ? "Signal ".($? & 127) : '';
  my $core = ' (dumped core)' if $? & 128;
  my $error = $?>>8 ? "ERROR(".($?>>8).") " : '';
  _print('W', "got $error$signal$core\n\twhile executing @cmd") if $?;
  &_print('D', "") if $signal;

#  $res contains both messages and result
  my ($pos, $k);
  $pos=$[;
  while(0 <= ($k = index($res,"\n",$pos))) { $pos = $k+1; }
  print substr($res,$[,$pos) if ($pos);
  return substr($res,$pos);
}

sub use_arc {
  @_ ? ($want_arc = shift) : return $want_arc;
}



package Vol_Do;
use Vos;
import Volset qw( is_volset vol2set set2pat volset_quota volsetopts
                  decode_quota drain_partition pools get_pooldisks
                  is_partition get_afsservers whoami get_obsolete_partitions get_readonly_partitions
                );

sub __volmove {

  my $maxrc = 0;
  my $to = ($#_) ? pop @_ : "";    # if number of arguments > 1
  my @vols = @_;
  &_print('D', "no volume name or volume ID given") unless @vols;
  Volset::read_cf();

  while (@vols) {
    my($errmsg, $volume, $volname, $rwvol, $isreadonly, $noaction, $from, $quota, $pve, @parts)
        = argcheck(\@vols);
    if ($errmsg) {
      _print('E', "$errmsg");
      next;
    }

    my $would = $noaction ? ' would' : ' will';
    my $blocks = &get_volattrib($volname,'Blocks');
    if ( $blocks > (2*1024*1024-100)) {
##      _print('E', "attempt to move volume $volume > 2GB") unless $Volset::opt_f;
##      return 1 unless $Volset::opt_f;
      _print('W', "about to move volume $volume > 2GB");
    }

    my $rwpart = shift @parts;                    # RW volume is always the first one
    (my $rwsitepart = $rwpart) =~ s/\// /;
    my $replicated = $#parts >= 0;
    my $vols_atrw = grep {$_ eq $rwpart} @parts;
# are we asked to move R/O volume ?
    if ( $isreadonly ) {         # we treat it as create_replica, delete_replica
      if (defined $to and grep { $_ eq $to} @parts) {
         _print('W', "target $to already contains a copy of $volume");
         next;
      }
      if ($from) {
        unless (grep {$_ eq $from} @parts) {
          _print('W', "volume $volume not on partition $from");
          next;
        }
        if ($from eq $rwpart) {
          _print('W', "volume $volume on R/W partition. should move R/W volume first");
          next;
        }
      } else {
        # check first if we can move R/O volumes from a partition to be drained
        if (my %drain = drain_partition()) { 
          my @drain = keys %drain;
          my @qarts = @parts;
          @parts = ();
          foreach my $a (@qarts) { 
             if ( grep {$_ eq $a} @drain ) { unshift @parts, $a; }
             else { push @parts, $a; }
          }
        }

        while (@parts) {
          $from = shift @parts;
          last unless $from eq $rwpart;
        }
      }

      if ( $from eq $rwpart or ! $from ) {
        _print('W', "request to move $volume from $rwpart(RW site) skipped");
        next;
      }
      if ( $vols_atrw ) {
        $to = __choose_disk($volname, $to, $blocks) if $to !~ /\//;
        unless($to) {
          _print('E', "no suitable disk found");
          next;
        }
      } else {
        $to = $rwpart unless $to;
      }
      _print("$would add site $to and remove $from for $volname") if $Volset::opt_v or $noaction;
      (my $tos_p = $to) =~ s/\// /;
      my $rc = vos_execute("addsite $tos_p $volname", $noaction, 2);
      $maxrc = $rc if $rc > $maxrc;
      $rc = &__vol_release($volname);
      $maxrc = $rc if $rc > $maxrc;
      (my $froms_p = $from) =~ s/\// /;
      $rc = vos_execute("remove $froms_p $volname.readonly", $noaction, 2);
      $maxrc = $rc if $rc > $maxrc;

    } else {                  # request to move RW volume
      $from = $rwpart;
      if ($replicated and not $vols_atrw) {
        # move an isolated R/W volume to a site where a R/O partition is found
        $to = pop @parts unless grep {$_ eq $to} @parts;
      } else {
        # otherwise we have to move a R/W volume (and usually a R/O volume as well)
        $to = __choose_disk($volname, $to, $blocks) if $to !~ /\//;
        unless($to) {
          _print('E', "no suitable disk found");
          next;
        }
      }
      (my $tos_p = $to) =~ s/\// /;
      _print("$would move $volume from $rwpart to $to") if $Volset::opt_v or $noaction;
      my $rc = vos_execute("move $volname $rwsitepart $tos_p", $noaction, 2);
      $maxrc = $rc if $rc > $maxrc;
# create a backup volume if the original RW volume had one
      $rc = vos_execute("backup $volname", $noaction, 2)
             if &get_volattrib($volname, 'BKVol');
      $maxrc = $rc if $rc > $maxrc;
# add RO site if original volume was replicated
      if ( $replicated ) {
        if ( $vols_atrw ) {
          _print("$would add site $to and remove $rwpart for $volname") if $Volset::opt_v or $noaction;
          my $rc = vos_execute("addsite $tos_p $volname", $noaction, 2);
          $maxrc = $rc if $rc > $maxrc;
          $rc = &__vol_release($volname);
          $maxrc = $rc if $rc > $maxrc;
          $rc = vos_execute("remove $rwsitepart $volname.readonly", $noaction, 2);
          $maxrc = $rc if $rc > $maxrc;
        } else {
          $rc = &__vol_release($volname);
          $maxrc = $rc if $rc > $maxrc;
        }
      }
    }

    unless ( $noaction or $maxrc ) {
      new_committed($to, $quota);
      new_committed($from, -$quota);
      my $vio = __IOpenalty($volname);     #I/O penalty for volume
      if ( $replicated ) {
        # we consider all I/Os come from the replicas, so if moved a replica with the R/W
        #  (vols_atRW set), we consider we moved 1/(nb_sites-1) of the total I/Os
        #  else we did not move any I/Os.
        if ( $vols_atrw ) {
          my $k = $#parts || 1;
          $vio /= $k;
        } else {
          $vio = 0;   
        }
      }
      if ( $vio ) {            
         my $Vfrom = __IOpenalty($from) - $vio;
         (my $zf = $from) =~ s/\//./;
         &sys_exec("echo $Vfrom > $QVDIR.srv/$zf");
         my $Vto   = __IOpenalty($to)   + $vio;
         (my $zt = $to)   =~ s/\//./;
         &sys_exec("echo $Vto   > $QVDIR.srv/$zt");
      }
      purge_diskinfo($to);
      purge_diskinfo($from);
      purge_volinfo($volname);
    }
  }
  return $maxrc;
}

sub __vol_release {
  my($errmsg, $volume, $volname, $rwvol, $isreadonly, $noaction, $nil, $quota, $pve, @parts)
      = argcheck(\@_,1,"vosrelease");               # skip from_or_to
  if ($errmsg) {
    _print('E', "$errmsg");
    return undef; 
  }

  my $f = shift @_ if defined $_[0];
  my $vname = &get_volattrib($volume, 'Name');
  my $vid   = &get_volattrib($volume, 'VolID');
  my $type  = &get_volattrib($volume, 'Voltype');
  unless ($type =~ /(RO|RW)/) {
    if ($f) {
      _print('W', "cannot determine RO/RW status of $f");
    } else {
      _print('W', "cannot determine RO/RW status of $volume");
    }
  }


#  for a RO volume, extract the corresponding RW volume
  if ($isreadonly) {
    if ($Volset::opt_v) {
      if ($f) {
        _print('W', "$f is on read-only volume $vname ($vid),");
      } else {
        _print('W', "volume $vname ($vid) is read-only,");
      }
      _print('W', "\n\tread-write volume is $rwvol");
    }
#  for a RW volume, see if RO copies exist
  } else {
    my $w = $vid if $#parts;    # @parts has 2 or more elements
    if ($Volset::opt_v) {
      if ($f) {
        _print('W', "$f is on read-write volume $volname ($rwvol)");
        _print('W', "\n\twhich has read-only copies") if $w;
      } else {
        _print('W', "volume $volname ($vid) is read-write");
        _print('W', "\n\tand has read-only copies") if $w;
      }
    }
    return undef unless $w;
  }

#  if RO volume exist, vos release the RW one
   
  my $index;
  my $string;
  for ($index=1, $index <10, $index++) {
    $string = Vos::vos_execute("release $volname -v",$noaction);       # no verbosity!
    if ($string=~/Possible communication failure/) {
      _print('W', "release failed for at least one replica") if $Volset::opt_v;
      sleep 5;
      next;
    }
    if ($string=~/ successfully$/) {
      _print("release of $volname done");
      return 0;
    } elsif ($noaction) {
      return 0;
    } else {
      last;
    }
    last;
  }
  _print('E', "release of $volname failed, last message is", "\n\t$string");
  return 1;
}

sub __vol_create {
  my($volname,$to,$quota) = (@_,0,0);

  return (undef) unless $volname;
  my $noaction = &noaction();
  my $exa = exa_single_volumes(1);
  if (my @c = examine($volname)) {              # volume already exists
    _print('E', "volume $volname already exists");
    exa_single_volumes($exa);
    return undef;
  }
  my(@pventry)=&getpvbyvol($volname);    # check authorization, returns pattern, set, , proj_quota
  exa_single_volumes($exa);

  $quota = &volsetopts('newquota',$pventry[1]) unless $quota;
  $quota = decode_quota($quota, $volname);
  my $def_ios = volsetopts('newios',$pventry[1]);
  my $current = &part_quota(@pventry);
  if ($pventry[3] != 0 and ($current + $quota) > $pventry[3]) {
    _print('E', "$volname would exceed total quota for $pventry[1], current=$current, max=$pventry[3]");
    return undef;
  }

  unless ($to = __choose_disk($volname, $to, $quota, undef)) {
    _print('E', "No suitable partition found for $volname");
    return undef; 
  }
  (my $tos_p = $to) =~ s/\// /;
  my  $rc = vos_execute("create $tos_p $volname -verbose -maxq $quota",$noaction,2);
  unless($noaction or $rc) {
    purge_volinfo($volname);
    &new_committed($to, $quota);
    &sys_exec("echo $quota > $PVDIR/$volname");
    &sys_exec("echo $def_ios > $QVDIR/$volname");
    my $IOpenalty = __IOpenalty($to) + $def_ios;
    (my $tos_d = $to) =~ s/\//./;
    &sys_exec("echo $IOpenalty > $QVDIR.srv/$tos_d");
  }
  return $rc;
}

sub __vol_delete {
  my($errmsg, $volume, $volname, $rwvol, $isreadonly, $noaction, $from, $quota, $pve, @parts)
      = argcheck(\@_);
  if ($errmsg) {
    _print('E', "$errmsg");
    return undef;
  }
  $from = shift @_ unless $from;
  return (undef) unless $from;
  return undef unless grep {$_ eq $from} @parts;
  my $ROatRW = ((&get_volattrib($volume,'Voltype') eq "RO") and ($from eq $parts[0]));

  (my $froms_p = $from) =~ s/\// /;
  my $rc = &vos_execute("remove $froms_p $volume -verbose",$noaction,2);  # use volume, not volname
  unless($noaction or $rc) {
    purge_volinfo($volname);
    new_committed($from, -$quota)
        unless $ROatRW;

#       note on I/O impact
#       If we remove a volume, we should do "as-if" it never exited and decrement the partition's
#         global count by the volume's count. This way, a volume severely impacting a partition
#         would leave a decent I/O count when deleted or moved out.
#       However, since we only remember ONE volume's count, we have a problem in the case of
#         R/O replicas, as we do not know how much to take out from each partition.

#   volume global quota
    my $volQ = $quota * &part_unique(@parts);
    $volQ = $volQ -$quota unless $ROatRW;
    if ($volQ) {
        &sys_exec("echo $volQ > $PVDIR/$volname");
        &sys_exec("touch $QVDIR/$volname");
    } else {
        unlink "$PVDIR/$volname";
        unlink "$QVDIR/$volname";
    }
  }
  return $rc;
}

sub __vol_create_replica {
  my($errmsg, $volume, $volname, $rwvol, $isreadonly, $noaction, $to, $quota, $pve, @parts)
      = argcheck(\@_);
  if ($errmsg) {
    _print('E', "$errmsg");
    return undef;
  }
  $to = shift @_ unless $to;

  my $rwpart = shift @parts;             #  R/W is first element of list
  if ( grep {$_ eq $rwpart} @parts ) {
  # RO site and RW site already on same volume
    $to = __choose_disk($volname, $to, $quota);
  } else {
    _print('W', "no R/O volume yet at $rwpart (R/W site)\n",
          "\tIgnoring explicitly specified pool/server/partition $to")
          if $to and $to ne $rwpart;
    $to = $rwpart;
  }

  ## awi 16 nov 2010: don't enforce the first replica to be with the R/W
  ##                  as this may be on a different type of partition, e.g.
  ##                  RAID5 vs RAID1 or even SSD; use 'choose_disk' instead
  #$to = __choose_disk($volname, $to, $quota);
 
  unless ($to) {
    _print('E', "No suitable partition found for $volume");
    return undef; 
  }

  (my $tos_p = $to) =~ s/\// /;
  my $rc = &vos_execute("addsite $tos_p $volname -verbose",$noaction,2);
  unless($noaction or $rc) {
    purge_volinfo($volname);
    &new_committed($to,$quota) unless $to eq $rwpart;
    my $volQ = $quota * &part_unique($rwpart, @parts, $to);
    &sys_exec("echo $volQ > $PVDIR/$volname");
    &sys_exec("touch $QVDIR/$volname");

    $rc = &__vol_release($volname);
  }
  return $rc;
}

sub __vol_rename {
  my($errmsg, $vol1, $volname, $rwvol, $isreadonly, $noaction, $nil, $quota, $pve, @parts)
      = argcheck(\@_,1);                # skip from_or_to
  if ($errmsg) {
    _print('E', "$errmsg");
    return undef;
  }

  my($vol2) = shift @_;
  return (undef) unless $vol2;
  my $exa = exa_single_volumes(1);
  if (my @c = examine($vol2)) {                 # volume already exists
    _print('E', "volume $vol2 already exists");
    exa_single_volumes($exa);
    return undef;
  }
  exa_single_volumes($exa);
  my(@pventr2)=&getpvbyvol($vol2);       # check authorization, returns pattern, set, , proj_quota
  my(@pventry) = @$pve;

  my $volQ = $quota * &part_unique(@parts);

  if ($pventry[0] ne $pventr2[0]) {
# volume changes quota group, but don't let them play quota tricks...
    my $current = &part_quota(@pventr2);
    &_print('D', "$vol2 would exceed quota for $pventr2[1], current=$current, max=$pventr2[3]")
        if ($current + $volQ) > $pventr2[3] and $pventr2[3] != 0;;
  }

  my $rc = &vos_execute("rename $volname $vol2 -verbose",$noaction,2);
  unless ($noaction or $rc) {
    purge_volinfo ($volname);
    purge_volinfo ($vol2);
    &sys_exec("mv $PVDIR/$volname $PVDIR/$vol2");
    &sys_exec("mv $QVDIR/$volname $QVDIR/$vol2");
  }
  return $rc;
}

sub __vol_quota {
  my($errmsg, $volume, $volname, $rwvol, $isreadonly, $noaction, $nil, $quota, $pve, @parts)
      = argcheck(\@_,1);                # skip from_or_to
  if ($errmsg) {
    _print('E', "$errmsg");
    return undef;
  }

  my $old_quota = $quota;
  $quota = shift @_;
  my $volN = &part_unique(@parts);
  my(@pventry) = @$pve;

  $quota = (defined $main::opt_q) ? $main::opt_q : &volsetopts('newquota',$pventry[1]) unless($quota);
  $quota = decode_quota($quota, $volname);
  return 0 if $quota == $old_quota;             # no changes
  my $current = &part_quota(@pventry);
  &_print('D', "$volume would exceed total quota for $pventry[1], current=$current, max=$pventry[3]")
      if $pventry[3] != 0           # unless unlimited quota
      && $quota > $old_quota        # and unless shrinking
      && ($current+($quota-$old_quota)*$volN) > $pventry[3];

# Everything ok, create a temporary mountpoint and set quota
  my $rc = 0;
  if ($noaction) {
    _print("would create a temporary mount point and set quota to $quota");
    $rc = 99;
  } else {
    my $fs = fullpath('fs', $Volset::PGMLOC);
    my $tmp ;
    while(1) {
      $tmp="$TMPDIR/" . time();
      last unless -e $tmp;
      sleep 1;
    }

    &sys_exec("$fs mkmount $tmp $volname");
    &sys_exec("$fs setquota -path $tmp -max $quota");
    $rc = $?;
    &sys_exec("$fs rmmount $tmp");

    my %seen;
    foreach (@parts) {
      my $to = $_; $to =~ s/\//./;
      &new_committed($to, $quota-$old_quota) unless $seen{$_}++;
    }
    my $volQ = $quota * $volN;
    &sys_exec("echo $volQ > $PVDIR/$volname");
    &sys_exec("touch $QVDIR/$volname");
  }
  return $rc;
}



sub __choose_disk {
  my ($vol, $to, $blocks, $exclude) = (@_, "", "", "");
  my ($volname, @excl, @incl, @afsparts);
  _print("called with vol=$vol, to=$to, blocks=$blocks" ,
        ($exclude eq "N") ? " ,exclude=N" : "")
             if $Volset::opt_v>2;

  my $exa = exa_single_volumes(1);
  if (my @c = examine($vol)) {
    my $rwvol = &get_volattrib($vol, 'RWVol');
    unless ($rwvol and (my @c = examine($rwvol)) ) {
      exa_single_volumes($exa);
      return undef;
    }
    exa_single_volumes($exa);
    $volname = get_volattrib($rwvol,'Name');
    @afsparts = afsvol_part($volname);
    if ($exclude eq "N") {
      @excl = ();
    } else {
# exclude all partitions, that have already a RW or RO copy of the volume
      if ($#afsparts > 0) {  # there are R/O replicas, exclude complete servers
        my %excl;
        for my $p ( @afsparts ) {
          $excl{$1} = 1 if $p =~ /^([^\/]+)\//;
        }
        for my $s ( keys %excl ) {
          push @excl, partitions($s);
        }
	# make sure the volume is marked as a "readonly" for further down
	$vol .= '.readonly' unless ( $vol =~ /\.readonly$|\.backup$/)
      } else {               # else only exclude partition
        @excl = @afsparts;
      }
    }
  } else {
    ($volname = $vol) =~ s/\.readonly$|\.backup$//;
    @excl = ();
  }
  exa_single_volumes($exa);
  $blocks = 0 unless $blocks;

# exclude all partitions to be drained
  {
    local($Volset::opt_v) = 0;
    my @sets = &vol2set($vol);
    my %drain = drain_partition();
    for my $p ( keys %drain ) {
      push @excl, $p if defined $drain{$p}
                      and ($drain{$p} eq 0 or grep {$drain{$p} eq $_} @sets);
    }
  }
# exclude all partitions on servers marked as "obsolete".
  push @excl,  get_obsolete_partitions();
# exclude all partitions on servers marked as "readonly", unless
# we are looking at a readonly volume. Or a server-specific test volume
  unless ($vol =~ '.readonly$|^q\.afs\.afs') {
      push @excl,  get_readonly_partitions();
  }
  
  _print("exclude initially @excl as target for $vol")
        if $Volset::opt_v and @excl;
# partition given, do nothing
  if ( $to and is_partition($to)) {
    @incl = ( $to );
# server or pool given, get partitions
  } elsif ( $to ) {
    @incl = partitions($to) if grep {$to eq $_} get_afsservers('fs');
    @incl = get_pooldisks($to) unless @incl;
  }
# try to find partitions most suitable for the given vol
# first determine the possible volsets for a volume, then select the partition:
# 1. volset is mentioned in PARTITIONS section -> use that partition
# 2. select a partition that has already volumes from the same volset
# 3. select a partition with enough free space
  $to = choose_partitions($vol, \@excl, \@incl, $blocks, $exclude);
# as the pointer to @excl and @incl is global, we need to empty the arrays
  _print('E', "No suitable disk found for $vol") if $Volset::opt_v and ! $to;
  if ($exclude eq "N" and grep {$_ eq $to} @afsparts) {
    _print('W', "volume $vol already on best partition");
    $to = "";
  }
  return $to;
}

sub choose_partitions {
#
# constants:
#
#    strategy = free :    first sort on free space, then on number of sets per partition
#       select on bigger free space percentage
#         and smaller access-penalty
#           and on minimum number of sets per partition

#       nb_sets: -1  k/nb_sets:  0  %used: -1   I/Os: -1/100000

#    strategy = cluster : select within partitions already containing same set.
#       first sort by number of volumes of that set & by number of sets par partition, 
#         then by bigger free space percentage and smaller access-penalty

#       nb_sets:  0  k/nb_sets: 25  %used: -1   I/Os: -1/100000

#    strategy = cluster+free  : similar to cluster, but with less emphasis on clustering

#       nb_sets:  0  k/nb_sets: 05  %used: -1   I/Os: -1/100000

#    strategy = iobalance : similar to free, but with more emphasis on i/o counts

#       nb_sets: -1  k/nb_sets:  0  %used: -1   I/Os: -1/10000


  my($x) = exa_single_volumes(0);
  my($debug) = ($Volset::opt_v>2);
  my ($vol, $excldisks, $incldisks, $blocks, $exclude) = @_;
  _print("find best partition for $vol from @$incldisks")
        if $Volset::opt_v and $incldisks and @$incldisks;
  _print("exclude @$excldisks")
        if $Volset::opt_v and $excldisks and @$excldisks;
  my %incldisks = map { $_, 1 } @$incldisks if $incldisks;
  my %excldisks = map { $_, 1 } @$excldisks if $excldisks;
  my @already = afsvol_part($vol);    # non empty is exists and exclude==N
# for all possible volsets (derived from the vol_name) in turn:
  my @sets = reverse &vol2set($vol);
  my $o_set = $sets[0];    # remember the most specific set
  my $strategy = &volsetopts('strategy', @sets);     # this one doesnot change while we percolate
  while ( @sets ) {
    my $max_used = &volsetopts('space', @sets);
    my $max_comm = &volsetopts('committed', @sets);
    my $max_used_by_set = &volsetopts('spaceset', @sets);
    my $max_comm_by_set = &volsetopts('committedset', @sets);
    my $max_volumes_by_set = &volsetopts('numvolsset', @sets);
    my $def_ios  = &volsetopts('newios', @sets);
    my $set = shift @sets;       #   !!! MUST not precede any use of volsetopts
    _print("trying with set: $set",
          "\n                 maximum space usage            : $max_used%",
          "\n                 maximum overcommittment        : $max_comm%",
          "\n                 maximum space usage by set     : $max_used_by_set%",
          "\n                 maximum overcommittment by set : $max_comm_by_set%",
          "\n                 maximum num vols by set        : $max_volumes_by_set",
          "\n                 part selection strategy        : $strategy")
          if $Volset::opt_v;
# get the partitions associated with a project as described in afsadmin.cf
    my @parts = get_pooldisks($set);
    next unless @parts;
# use the partitions defined for the most specific volset
    if ( $excldisks and @$excldisks ) {
      my @d = grep { ! defined $excldisks{$_}} @parts;
      @parts = @d;
    }
    if ( $incldisks and @$incldisks ) {
      my @d = grep { defined $incldisks{$_}} @parts;
      @parts = @d;
    }
# not a pool or all partitions excluded
    next unless @parts;
    _print("$set contains @parts") if $Volset::opt_v;

    # fill the attractiveness hash from the persistent parameters
    my %Hattractiveness = ();   
    if ((-r "/p/var/volume_parameters/$vol") && (open(F, "</p/var/volume_parameters/$vol"))) {
        _print "Open persistent parameters file" if $Volset::opt_v;
        while (<F>) {
                chomp $_;
                _print "Processing parameter \'$_\'\n" if $Volset::opt_v;
                my ($cmd, $vol, $opt) = split(/\s+/, $_);
                # if we have a pattern, get the corresponding volumes
                # (but 20 at max)
                my @Avols = ();
                if ($vol =~ /\*$/) {
                        my $i = 0;
                        foreach (</var/afs_admin/pv/$vol>) {
                                my ($volfile, $x) = fileparse($_);
                                push @Avols, $volfile;
                                last if (scalar @Avols >= 20);                                          
                        } 
                } else {
                        push @Avols, $vol;
                }
                _print "Avols has ". scalar @Avols . " entries\n" if $Volset::opt_v;
                foreach $vol (@Avols) {
                        _print "vol is $vol (2)\n" if $Volset::opt_v;   
                        my @Aloc = afsvol_part($vol);
			# ignore the location of the R/W when we have a parameter
			# for a R/O
			if ($vol =~ /readonly$/) {
			    shift @Aloc;
			}
                        if ($cmd eq "with") {
			    if ($opt eq "--server") {
				foreach my $loc (@Aloc) {
				    my $srv = (split(/\//, $loc))[0]; 
				    foreach my $p (partitions($srv)) {
					if (exists $Hattractiveness{$p}) {
					    $Hattractiveness{$p} += 1;
					} else {
					    $Hattractiveness{$p} = 1;
					}
				    }
				}
			    } else {
				foreach my $loc (@Aloc) {
				    if (exists $Hattractiveness{$loc}) {
					$Hattractiveness{$Aloc[0]} += 1;
				    } else {
					$Hattractiveness{$Aloc[0]} = 1;
				    }
				}
			    }
                        } elsif ($cmd eq "not_with") {
			    if ($opt eq "--server") {
				foreach my $loc (@Aloc) {
				    my $srv = (split(/\//, $loc))[0]; 
				    foreach my $p (partitions($srv)) {
					if (exists $Hattractiveness{$p}) {
					    $Hattractiveness{$p} -= 1;
					} else {
					    $Hattractiveness{$p} = -1;
					}
				    }
				}
			    } else {
				foreach my $loc (@Aloc) {
				    if (exists $Hattractiveness{$loc}) {
					$Hattractiveness{$Aloc[0]} -= 1;
				    } else {
					$Hattractiveness{$Aloc[0]} = -1;
				    }  
				}
			    }
                        }
                }
        }
    }

    _print "Attractiveness hash start" if $Volset::opt_v;
    while ( my ($k,$v) = each %Hattractiveness ) {
	    _print "Attractiveness $k => $v" if $Volset::opt_v;
    }
    _print "Attractiveness hash end" if $Volset::opt_v;
 
    my($part,%w1);
    foreach $part (@parts) {
      my ($nb_sets,$capa,$pu,$pc,$pus,$pcs,$pi,$k,$attr);
      $w1{$part} = 0;
      $nb_sets = scalar pools($part) || 1;
      $capa = disk_capacity($part) || 1;

      # in we consider partitions already containing volume, do not count it twice
      my $x_blocks = $blocks;
      my $IOpenalty = __IOpenalty($vol);
      if ( $IOpenalty eq "" ) { $IOpenalty = $def_ios }
      if (grep {$_ eq $part} @already) {
         $x_blocks = 0;
         $IOpenalty = 0;
      }

      $pu   = int(100 - free_percent($part) + $x_blocks*100/$capa);
      $pc   = int(100 * ( get_partattrib($part , 'Committed' ) || 0 )/$capa);
      $pus  = 0;
      $pcs  = 0;

      if ($set eq 'users' or $set eq 'work') {
        # compute the used and committed amounts including only the volumes in this set
        my ($s,$p) = split /\//,$part;
        my $pattern = '';
        if ($set eq 'users') {
          $pattern = 'user.%';
        } elsif ($set eq 'work') {
          $pattern = 'work.%';
        }

        use ConsoleDB;
        my $dbh = ConsoleDB::__connect();

        my $qry = $dbh->prepare("select sum(size) from volumes where server='$s' and vice_partition='$p' and recent=1 and volumename like '$pattern'");
        $qry->execute();
        $qry->bind_columns(undef,\$pus);
        $qry->fetch();
        $qry->finish();
        $pus = int(100 * $pus / $capa);

        $qry = $dbh->prepare("select sum(quota) from volumes where server='$s' and vice_partition='$p' and recent=1 and volumename like '$pattern'");
        $qry->execute();
        $qry->bind_columns(undef,\$pcs);
        $qry->fetch();
        $qry->finish();
        $pcs = int(100 * $pcs / $capa);

        $dbh->disconnect();
      }

      $k    = part_count($set, $part) || 0;
      if (defined $Volset'Partitions{$part}->{Speed} and $Volset'Partitions{$part}->{Speed} > 5) {
	# penalty approaching zero asymptotically with increasing I/O per byte
	# C = 0.1 (scaling, arbitrary)
	# v = speed, s = size in blocks in MB, at least 1024
	# penalty(iopenalty) = f(x) = C*s/(v*x)
	my $v = $Volset'Partitions{$part}->{Speed};
	my $s = &get_volattrib($vol,'Blocks')>>10;
	$s = 1024*1024 unless defined($s) && $s > 1024*1024;
	$IOpenalty = 0.01 unless $IOpenalty > 0;
	$pi = (0.1 * $s) / ( $IOpenalty * $v);
	my $partIOpenalty = __IOpenalty($part);
	_print("pi=$pi, IOpenalty=$IOpenalty, partIOpenalty($part)=$partIOpenalty, s=$s, v=$v") if $Volset::opt_v > 5;
	$pi += $partIOpenalty / $v;
      }
      elsif (defined $Volset'Partitions{$part}->{Speed} and $Volset'Partitions{$part}->{Speed}) {
	# a square penalty penalizing "average" volumes with non-zero but uninteresting I/O counts
	# parabola with f(0) = f(2m) = 0, f(m) = P
	# penalty(iopenalty) = f(x) = -P/m^2 * x^2 + 2P/m * x = Px/m * (2 - x/m)
	# P = 86400 (arbitrary), m = 5*speed
	my $m = 5 * $Volset'Partitions{$part}->{Speed};
	my $P = 86400;
	my $x = ($IOpenalty >=0 && $IOpenalty < 2*$m) ? $IOpenalty : 0;
	my $xm = $x / $m;
	$pi = $P * $xm * (2 - $xm);
	_print("pi=$pi IOpenalty=$IOpenalty P=$P m=$m pi=$pi") if $Volset::opt_v > 5;
	$pi += __IOpenalty($part);
      } else {
	$pi   = __IOpenalty($part) + $IOpenalty;
      }

      # add the attractiveness of a partition (as specified by the volume creator)
      $attr = (exists $Hattractiveness{$part}) ? $Hattractiveness{$part} : 0;

      _print("partition $part has $k volumes and $nb_sets sets , " ,
                 " $pu percent used",      ($pu>$max_used ? " (skipped)" : "" ), " , " ,
                 " $pc percent committed", ($pc>$max_comm ? " (skipped)" : "" ), " , " ,
                 " $pus percent used by set",      ($pus>$max_used_by_set ? " (skipped)" : "" ), " , " ,
                 " $pcs percent committed by set", ($pcs>$max_comm_by_set ? " (skipped)" : "" ), " , " ,
                 " $k num vols by set", ($k>$max_volumes_by_set ? " (skipped)" : "" ), " , " ,
                 " I/O penalty: $pi, ",
	         " attractiveness: $attr")
         if $debug or ($Volset::opt_v and $pc>$max_comm) or ($Volset::opt_v and $pu>$max_used);
      if (($pu > $max_used) or ($pc > $max_comm) or ($pus > $max_used_by_set) or ($pcs > $max_comm_by_set) or ($k > $max_volumes_by_set)) { # if overcommitted or not enough space
         $w1{$part} = -999999999;
         next;
      }

      $w1{$part} = $Volset'Str_fact{$strategy}{'k'} * $k/$nb_sets	# select on partitions already containing that set
                 + $Volset'Str_fact{$strategy}{'pu'} * $pu              # then on bigger free space percentage
                 + $Volset'Str_fact{$strategy}{'io'} * $pi              # and smaller access-rate-penalty
                 + $Volset'Str_fact{$strategy}{'nbs'} * $nb_sets                  # and on minimum number of sets per partition
		 + $Volset'Str_fact{$strategy}{'attr'} * $attr;                       # and on greater attractiveness
      _print("partition $part has weight $w1{$part} ($k,$nb_sets,$pu,$pi,$attr)\n") if $Volset::opt_v;
    }

    my @newdisks = sort { $w1{$b} <=> $w1{$a} } @parts;
    exa_single_volumes($x);
    my $which = 0;
    # if more than two eligible, random over the top ones of "similar" weight
    _print("$#newdisks eligible partitions:\n") if $Volset::opt_v;
    if ($#newdisks > 2) {
	while ($which < $#newdisks/2 && $which < 11 &&
	    ($w1{$newdisks[$which+1]}-$w1{$newdisks[$which]}) > $w1{$newdisks[0]}) {
                _print("    $newdisks[$which] ($w1{$newdisks[$which]}) is eligible") if $Volset::opt_v;
		$which++;
	}
        _print("    $newdisks[$which] ($w1{$newdisks[$which]}) is eligible") if $Volset::opt_v;
        _print("    choosing randomly between top ".($which+1)." partitions\n") if $Volset::opt_v;
        _print("    next partition ($newdisks[$which+1]) has weight $w1{$newdisks[$which+1]}\n") if $Volset::opt_v;
	$which = int(rand($which+1)) if $which > 0;
    }
    return $newdisks[$which] unless $w1{$newdisks[$which]} == -999999999 ;
  }
  exa_single_volumes($x);
  return undef;
}


sub argcheck {
  my ($arglist, $skip_from_to, $checklevel) = (@_,0,0);
  my ($errmsg, $volume, $volname, $rwvol, $isreadonly, $noaction, $from_or_to, $quota, $pve, @parts);
  return ("no volume specified") 
      unless $volume = shift @$arglist;
  $noaction = &noaction();
  my $exa = exa_single_volumes(1);
  unless (my @c = examine($volume)) {
    exa_single_volumes($exa);
    return ("volume $volume does not exist")
  }
  if (get_volattrib($volume,'Voltype') eq "BK") {
    exa_single_volumes($exa);
    return ("do not process backup volume $volume")
  }
  $rwvol  = &get_volattrib($volume,'RWVol');
  unless ($rwvol and (my @c = examine($rwvol))) {
    exa_single_volumes($exa);
    return ("cannot locate the R/W volume for $volume");
  }
  exa_single_volumes($exa);
  $volname= &get_volattrib($rwvol,'Name');
  $isreadonly = (&get_volattrib($volume, 'Voltype') eq 'RO');
  unless ($skip_from_to) {
    if (defined $$arglist[0] and $$arglist[0] =~ /\//) {
      $from_or_to = shift @$arglist;
    }
  }
  my @pventry = &getpvbyvol($volname,$checklevel);    # check authorization, returns pattern, set, , proj_quota
  $quota = &get_volattrib($rwvol, 'Quota');
  @parts   = afsvol_part($volname);
  return ("", $volume, $volname, $rwvol, $isreadonly, $noaction, $from_or_to, $quota, \@pventry, @parts);
}


# this function NOW (march 2011) returns a value between 0 (light use) to 100 (heavy use)
# changed by Dan to use ConsoleDB Nov 28 2012
sub __IOpenalty {
  my ($object) = @_;
#  unless ($ENV{'ORACLE_HOME'}) { require "/p/etc/run_oracle.pl"; }
#  require "/p/perlmodules/AFSPerf.pm" unless defined(&AFSPerf::accessTimeRead);
  require ConsoleDB unless defined(&ConsoleDB::partitions_readaccesstime);
  my $res;

  if ($object =~ /\//) {
#	$res = &AFSPerf::accessTimeRead($object,24) + &AFSPerf::accessTimeWrite($object,24);
	$res = &ConsoleDB::partitions_readaccesstime($object,24) + &ConsoleDB::partitions_writeaccesstime($object,24);
  _print(" xxx res($object)=$res") if $Volset::opt_v > 9;
	# 50 ms = heavy use -> 100, $res returned in microseconds.
	$res = int($res / 1000 * 2 + 0.5);
  } else {
#	$res = &AFSPerf::accessRateRead($object), &AFSPerf::accessRateWrite($object);
	$res = &ConsoleDB::volumes_readaccessrate($object) + &ConsoleDB::volumes_writeaccessrate($object);
  _print(" yyy res($object)=$res") if $Volset::opt_v > 9;
	# 50 rw/s = heavy use -> 100
	$res = int($res / 50 * 100 + 0.5);
  }

  if ($res > 100) { $res = 100; } 
  if ($res<0) {$res = 0;}	
  _print("res($object)=$res") if $Volset::opt_v > 7;
  return $res;              # return aggregated access count. will need re-scaling
}


sub getpvbyvol {
# see "THE AUTHORIZATION SCHEMA" for the meaning and allowed values of $skipchk
  my($vol,$skipchk)=(@_,0);
  &_print('D', "Error, volume not specified") unless $vol;
  local($Volset::opt_v) = 0;
  my (@sets, $Squota, $pv, $qset);

# start with the most specific one and get quota and setname
  @sets = &vol2set($vol);
  for my $set ( reverse @sets ) {
    if ( ! defined $Squota ) {
      my $q = volset_quota($set);               # maximum quota for volset
      $Squota = decode_quota($q) if $q;
    }
    $qset = $set if $Squota and ! $qset;
    next unless check_acl($set,$skipchk);
    $pv = $qset || $set;
    last if $Squota;
  }
  &_print('D', "No permission for $vol") unless $pv or ( $skipchk%2 );  # odd levels do not abort
  return (undef) unless $pv;
  my %pat = set2pat($pv);
  my $pat = join '|', keys %pat;
  
  return ( $pat, $pv, 0, $Squota );
}


sub part_quota {
  my ($pat, $volset) = @_;
  my ($quota, $q, $f);
  local($Volset::opt_v) = 0;

  opendir D, $PVDIR || &_print('D', "$PVDIR: $!");
  file: while ( $f = readdir D ) {
    next unless( $f =~ /$pat/ );
    if ( $volset ) {
      for my $set ( reverse &vol2set($f) ) {
        last if $set eq $volset;
        next if $set =~ /^__[US\@]$/;
        next file if is_volset($set);
      }
    }
    open F,("$PVDIR/$f") || &_print('D', "$f: $!");
    $q = <F>;
    close F;
    if ($q) {
      chomp $q ;
      $quota += $q;
    } else {
      $q = &get_volattrib($f,'Quota');
      $quota = $q * &part_unique(afsvol_part($f));
    }
  }
  closedir D;
  return($quota);
}

sub part_unique {
  my (@parts) = @_;
  my ($n, %seen);
  foreach (@parts) {
     $n++ unless $seen{$_}++;
  }
  return $n;
}

sub part_count  {
  Volset::read_cf();
  my $x = Volset::verbose();
  Volset::verbose(0);
  my($set,$part) = @_;
  my($c) = 0;
  my($v);
# vos_listvol($part);
  foreach $v (vos_listvldb($part)) {
     my(@s) = vol2set($v->{Name});
     if (grep {$_ eq $set} @s) { $c++ }
  }
  Volset::verbose($x);
  return $c;
}


sub check_acl {
# see "THE AUTHORIZATION SCHEMA" for the meaning and allowed values of $level

  my ($set,$level) = (@_,0); 
  my $string = "";
  unless ($level =~ /^\d+$/) { $string = $level; $level=9; }
  return 1 if $level == 1;                                        # anyone

  $set = lc($set);    # set all lowercases
  $arccaller = whoami() unless ($arccaller);
  return 1 if int($level/2) == 1 and $set =~ /^__$arccaller$/;    # level == 2 or 3

  my $pts = fullpath('pts', $Volset::PGMLOC);
  my $suid;
  if ($> == 0 and $< != 0) { # running in suid mode?
    $suid = $<;    # keep orig
    $<    = $>;    # become real root
  }
  my (@res) = `$pts mem -nameorid $arccaller -expandgroups 2>/dev/null`;
  (@res) = `$pts mem -nameorid $arccaller 2>/dev/null` unless @res;  # 1.4 did not support -expandgroups
  $< = $suid if $suid;  # revert
  shift @res;
  return 1 if grep { /\s+system:administrators\s*$/ } @res;
  return 1 if grep { /\s+afs:afs_admin\s*$/ } @res;

  if ($set) {
    return 1 if grep { /\s+_${set}_\s*$/ } @res;
    return 1 if $string and grep { /\s+_${set}_:_$string\s*$/ } @res;
    if ($set =~ /^(.*)_\w$/) { $set = $1; }
    return 1 if grep { /\s+_${set}_\s*$/ } @res;
  }
  return 0;
}
sub get_acl {
  my $pts = fullpath('pts', $Volset::PGMLOC);
  my $prj=$_[0];
  my $res;
  my $suid;
  if ($> == 0 and $< != 0) { # running in suid mode?
    $suid = $<;    # keep orig
    $<    = $>;    # become real root
  }
  if ($prj) {
    $res = `$pts mem -nameorid _${prj}_ -expandgroups 2>/dev/null`;
    $res = `$pts mem -nameorid _${prj}_ 2>/dev/null` unless $res;  # 1.4 did not have expandgroups
    unless ($res) {
      if ($prj =~ /^(.*)_\w$/) {
        $prj = $1;
        $res = `$pts mem -nameorid _${prj}_ -expandgroups 2>/dev/null`;
        $res = `$pts mem -nameorid _${prj}_ 2>/dev/null` unless $res;  # 1.4 did not have expandgroups
      }
    }
  } else {
    $res = `$pts mem -nameorid afs:afs_admin -expandgroups 2>/dev/null` .
           `$pts mem -nameorid system:administrators -expandgroups 2>/dev/null`;
    $res = `$pts mem -nameorid afs:afs_admin 2>/dev/null` .
           `$pts mem -nameorid system:administrators 2>/dev/null` unless $res;  # 1.4 did not have expandgroups
  }
  $< = $suid if $suid;  # revert
  return () unless $res;
  my @res = ();
  my %seen;
  foreach ( split /\n/, $res) {
    next if /^Members/;
    next if /^Expanded Members/;
    my ($i) = $_ =~ /\s+(\S+)\s*/;
    next if $seen{$i}++;
    push @res,$i;
  }
  return $prj, @res;
}


sub sys_exec {
  _print("$_[0]") if $Volset::opt_v;
  return(system($_[0]));
}

#sub vos_exec {
#  my $noaction = $_[1];
#  my $use_arc = use_arc();
#  if ( ! $use_arc ) {
#    return vos_execute(@_);
#  }
#  $arc = fullpath('arc',$Volset::PGMLOC) unless $arc;
#  my $cmd = "$arc -h $arcserver -s vos $_[0]";
#  my @cmd = split ' ', $cmd;    # do not go through the shell
#  _print $noaction ? "would " : "", "execute $cmd" if $Volset::opt_v or $noaction;
#  return 0 if $noaction;
#  system @cmd;
#  my $signal = $? & 127 ? "Signal ".($? & 127) : '';
#  my $core = ' (dumped core)' if $? & 128;
#  my $error = $?>>8 ? "ERROR(".($?>>8).") " : '';
#  _print('E', "got $error$signal$core\n\twhile executing $cmd") if $?;
#  &_print('D', "") if $signal;
#  return $?;
#}

package CacheCommitted;
sub cache_committed {
  # interface to cache the Committed value for each partition
  # returns cached value when called with one argument
  # caches new value when called with two arguments.
  my($part,$commit) = @_;
  (my $f = $part) =~ s/\//./;
  unless ($#_) {   # only one arg
    _print("called for $part") if $Volset::opt_v;
    $commit = 0;
    if ( open F, "$Volset::COMMDIR/$f") {
      chomp($commit = <F>);
      close F;
    }
    unless ($commit) {
      use Vos;
      vos_listvol ($part);      #  which recursively calls cache_committed
      $commit = $Vos::Part{$part}->{Committed};
    }
    return $commit;
  } else {         # two args, reset cache
    _print("called for $part & $commit") if $Volset::opt_v;
    if ($Volset::must_arc) {    # unless we are on a good machine
        Volset::arc_exec_pv(1, "cache_committed" , $part , $commit);  # cache value if $commit>0, purge cache if $commit==0
    } else {
      if ($commit == 0) {
        if (-e "$Volset::COMMDIR/$f") {
          unlink("$Volset::COMMDIR/$f") || _print('W', "cannot unlink $Volset::COMMDIR/$f: $!") ;
        }
      } else {
        open F,(">$Volset::COMMDIR/$f") || &_print('D', "cannot write to $Volset::COMMDIR/$f: $!") ;
        print F "$commit\n";
        close F;
      }
    }
  }
}
package Volset;     # back to the original package

$Volset::opt_v=0;
1;
__END__

=head1 NAME

Volset - Perl interface to manipulate a set of AFS volumes (AFSVolsets)

=head1 SYNOPSIS

  use Volset;
  Volset::verbose($value);
  %hash = get_volsets();
  $ok = is_volset($set);
  $value = volsetopts($name, @sets);
  @volsets = check_mount(\$mountpoint);
  @volsets = check_mount(\$mountpoint, $volset);
  @setnames = vol2set($volname_or_ID);
  @setnames = parentsets($set);
  @hosts = get_afsservers($servertype);
  @types = get_serverfeatures($server|$partition);
  @partitions = get_pooldisks(@setnames);
  @pools = pools($partition);
  $valid = is_partition($partition);
  $drain = drain_partition($partition);
  %drain = drain_partition();
  @poolnames = toplevel_pools();
  $disk = choose_disk($volume, , $blocks);
  $disk = choose_disk($volume, $server_or_partition, $blocks);
  $disk = choose_disk($volume, $server_or_partition, $blocks, $exclude, $attractive, $repulsive);
  $rc = volmove($volume);
  $rc = volmove(@volumes, $partition_or_volset);
  $homedir = homedir($user);
  move_2G_volumes($value);
  use_arc($value);

=head1 DESCRIPTION

This package heavily depends on a properly formatted configuration file
afsadmin.cf. Its location is contained in the package variable $Volset::CFLOC
and needs to be adjusted (edit the Volset.pm or modify $Volset::CFLOC after the
use Volset and before calling any function of that package

=over 4

=item C<homedir>

         $homedir = homedir($user);

return the home directory path for $user

=item C<get_volsets>

         %hash = get_volsets();

return a hash of the mount patterns (values) for all known volsets (keys)
as (described in afsadmin.cf, section [AFSVOLSETS])

=item C<is_volset>

         $ok = is_volset($set);

check if $set is a known volset name

=item C<volsetopts>

         $value = volsetopts($name, @sets);

retrieve the value for Volset $sets[0] option $name, where the option name
is currently one of the following keywords:
         backup
         committed
         newquota
         orphanedvols
         space

If the option for $sets[0] was not given in the config file, then the other
elements of @sets are tried in turn.

=item C<check_mount>

         @volsets = check_mount(\$mountpoint);
         @volsets = check_mount(\$mountpoint, $volset);

Add the cell prefix to the mountpoint if not supplied. Return a
list of volset names that are valid (according to the mountpoint patterns
from the configuration file) for the mountpoint.

If the volset input parameter is provided the mountpoint has to match the
pattern for the given volset.

=item C<vol2set>

         @setnames = vol2set($volname_or_ID);

Get all possible AFSVolset names a volume could be belonging to. The AFSVolset
names are sorted. The first name is the most general one, describing the top
level AFSVolset (pool) the volume belongs to. The last one is the most specific
volset.

=item C<parentset>

        @setnames = parentsets($set);

Return all volset names that are parents of the volset $set. This is determined
according to the definition in afsadmin.cf section [VOLSETPATTERNS].

=item C<get_afsservers>

         @hosts = get_afsservers($servertype);

Get the list of hosts that perform the AFS function described by $servertype.
More than one server type can be given in the list of arguments, in which case
all servers fulfilling all of the functions will be returned. The server type
names have to match the corresponding entries in the AFSSERVERS section of the
configuration file afsadmin.cf. Each of the arguments (which are ANDed) can
also be logical expressions using the operators & (for and), | (for or), !
(for not) and parentheses ().
Example: get_afsservers('fs&!db');

=item C<get_serverfeatures>

         @types = get_serverfeatures($server|$partition);

Returns the list of AFS functions defined for that server.

=item C<get_pooldisks>

         @partitions = get_pooldisks(@poolnames);

Get the list of AFS partitions ("hostname/partition", e.g. "afs1/c") that
are described to hold the volsets mentioned in the argument list. The arguments
have to match the corresponding entries in the PARTITIONS section of the
configuration file afsadmin.cf

=item C<toplevel_pools>

         @poolnames = toplevel_pools();

Get the list of top level pool names. Top level pool names are always 
associated with a partition on a file server.

=item C<pools>

         @pools = pools($partition);

return the pools defined for an existing partition on an AFS fileserver

=item C<is_partition>

         $valid = is_partition($partition);

return true if the argument is an existing partition on an AFS fileserver

=item C<drain_partition>

         $drain = drain_partition($partition);
         %drain = drain_partition();

return true (1) or the name of a volset if the partition should be drained.
The partition will not be selected as a target for creating or moving
volumes (belonging to the volset $drain or applies for all volumes).
If no partition is given, a hash is returned, where the keys are the 
partitions to be drained and the values are zero or volset names.

=item C<choose_disk>

         $disk = choose_disk($volume, , $blocks);
         $disk = choose_disk($volume, $server_or_partition, $blocks);
         $disk = choose_disk($volume, $server_or_partition, $blocks, $exclude, $attractive, $repulsive);

Select a partition name based on the name of the volume. First a list
of all possible volsets for that volume are determined. Then
partitions are selected that are foreseen for those volsets
[PARTITIONS] section in afsadmin.cf, while excluding those on
"obsolete" servers (as tagged in afsadmin.cf [SERVERS]. Servers tagged
as "readonly" will only receive readonly replicas, not RW volumes).

 If no partitions are found, then partitions are selected that
do already contain volumes belonging to the same volset.
If the volume is existing already then partitions containing RO or RW volumes
are excluded.
This procedure is repeated for all volsets from the most generic one to
the most specific one. The most specific list of partitions is then returned.
The optional $blocks parameter is used to limit selection to those partitions
having enough free space to accomodate at least that amount of data.
The (optional) parameter $exclude determines whether or not partitions that 
already hold a RW or a RO clone of the volume shall be excluded. This can
be suppressed by setting $exclude to \"N\". In all other cases choose_disk
will exclude partitions with a copy of the volume.
The (optional) parameters $attractive and $repulsive can be used to specify 
attractive (or repulsive) volumes, respectively. This will be used as a hint
for initial volume placement and allows for grouping (or spreading) of volumes.
The volume names are given as a comma separated list.   

=item C<volmove>

         $rc = volmove($volume);
         $rc = volmove(@volumes, $partition_or_volset);

Move volumes to a different partition. The partition is determined either
explicitely by a partition name or more indirectly by giving a file server
name or volset name. As a special case if only a single volume name is provided
and no target given at all, the volume is moved within the most specific volset.
If the partition is not explicitly specified, the least filled partition is
selected as the destination.

=item C<vol_release>

       $rc = vol_release($volume);
       $rc = vol_release(@volume, file_or_dir);

Issue a vos release on the target volume if given a R/W volume, or on the R/W parent
if given a R/O replica.
The vos release command is reissued up to 10 times if it fails with the message
"Possible communication failure", which generally means the fileserver did not
have enough time to break all callbacks.

=item C<use_arc>

         use_arc($value);

to execute commands that require special privileges do not execute them
directly but through the arc server, which grants privileges based on
configuration files. If you do not have arc, not calling this function
is safe. It only affects the behaviour of volmove. $value may be 0 or 1,
default is 0 (don't go through arc).

=item C<move_2G_volumes>

         move_2G_volumes($value);

allow moving volumes larger than 2GB which is not supported on some operating
systems. $value may be 0 or 1, default is 0 (don't allow). This function
affects the behaviour of volmove only. Currently this function does nothing,
volumes of any size will be accepted by volmove.

=item C<verbose>

         Volset::verbose($value);

set the verbose option to value. Presently only 0 and 1 are supported, default
is 0.


=back

=head1 THE AUTHORIZATION SCHEMA

As a script running in user mode cannot be trusted, validation & authorization is done in root mode,
either through direct call when user is already root on the same server, else by a remote call
through "arc".
The Volset module is consequently split into 2 logical pieces:  the Volset package, running in the 
mode of the caller, and the Vol_Do package, running as root on a dedicated server.
Validation & authorization is performed in Vol_Do, i.e. as root. It is based on matching paths and
volume names against "sets" defined in a configuration file (see afsadmin.cf below).

The main routines involved are getpvbyvol & check_acl. They both accept as extra argument an
authorization level.

=over 4

=item C<0>  (the default)
means that only designated administrators are allowed to perform some action

=item C<1>
means that everyone can perform some action

=item C<2>
means that in addtion to project's administrator, a user can perform some action against volumes
matching some specific set.

=item C<3>
means the same thing , but doesnot abort the script if the conditions are not matched.
(hint: 3 = 2+1, and odd authorization levels do not force an abort)

=item C<string>
means that in addtion to project's administrator, members of the _project_:_string protection group
can perform some action.

=back

=head1 THE CONFIGURATION FILE afsadmin.cf

The Volset module tries to provide an interface to the resources of the AFS
cell. It requires knowledge of the AFS cell structure, which could be partly
obtained using the AFS module. As some additional information is required
anyway, some of the information is repeated in the configuration file.

=over 4

=item C<layout of the configuration file afsadmin.cf>

afsadmin.cf consists of several sections which start with lines of the form
[SECTIONNAME], where SECTIONNAME currently is one of the names

   AFSCELL
   AFSSERVERS
   DRAIN
   PARTITIONS
   VOLUMEPATTERNS or
   AFSVOLSET

Lines containing a # character in column one and lines containing white space
only are skipped. The other lines need to follow a format specific to a section
(described below). All other lines contain one or more items separated by
white space unless otherwise noted.

=item C< Section AFSCELL>

contains one line with two items, the name of the AFS Cell and the homedir
construction rule. In the homedir construction rule $u will be replaced by
the username and $uN by the first N characters of the username.

Example:

      foo.com         /afs/foo.com/user/$u1/$u

=item C< Section AFSSERVERS>

contains lines with the items hostname, operating system, AFS version and 
server types. The OS and AFS version information is currently not used.
The domain part of host names will be chopped off, if it was given.
The servertypes are the strings you use as arguments to the get_afsservers
function. For file and database servers the strings fs and db should be used.
Other strings used at CERN are acron (cron with token), token (batch token
extender), arc (machine running the arc daemon), backup, dist and reg.
Non-production machines should be marked by I<- obsolete>.

Example:

      dbsrv1 Solaris OpenAFS1.2.3 fs db

=item C< Section DRAIN>

if a fileserver name or server/partition pair is given, then the partitions
mentioned will not be used to create volumes or move volumes there. If in
addition an AFSVolset (pool or project name) name is given, only volumes
belonging to the AFSVolset are affected.

Example:

      filesrv1
      filesrv2/aa scratch

=item C< Section PARTITIONS>

contains lines that describe which partitions are available and which pools
are associated with those partitions. If in addition project names are given,
then the processes of volume creation and movement respect those settings
as well. The lines contain partition names, pool names and project names.
Both pool and partition names are optional. An abbreviated notation can be
used to describe several partitions on the same server in one line.

Example:

      filesrv1/(a,b,c) scratch
      filesrv2/(aa,ab,ac) scratch project1
      filesrv3/a

=item C< Section AFSVOLSET>

Description of AFS volsets (projects and pools). The lines contain the fields
AFSVolset name, max quota, mount schema and flags. The project/volset name
is used to check permissions, assingn quotas and obtain statistics for a group
of related volumes. The max quota field describes the maximum amount of space
which can be allocated for this group of volumes (project, pool). The notation
with suffix k, M, G (,T) is allowed (a number without suffix means kBytes).
A quota of 0 means no limitation.
The mount schema is again a perl pattern, that must match the mount point
for the group of volumes. It is  prefixed with /afs/(.)cell_name/
Finally any number of flags in the form flagname=value (no blanks!) can be 
given on a line. Currently the following flags are recognized:

backup=1/0       (create or do not create backup volumes)

committed=n      (the sum of committed space for volumes on a given
                  partition may not exceed n percent, overcommittment
                  (n>100) allowed)

newquota=n       (by default create volumes with n kbytes quota)

newios=n         (pretend that new volumes already have done n I/Os)

orphanedvols=0/1 (the permission to create a mountpoint is derived from
                  the volume permissions of the current volume/from the
                  parent volume)

space=n          (the space currently used on a partition may mot exceed
                  n percent. committed and space are used for volume
                  creation)

strategy=string  (choose a strategy for creating new volumes or moving
                  volumes. Possible values for string are currently
                  free, cluster and free+cluster. See below for its meaning)

Examples:

      mail    2G project/mail
      exp    20G exp
      exp_s   5G 
      scratch  0 user/\w/\w+/(scratch)\d committed=100 backup=0 newquota=5000

=item C< Strategies for moving and creating volumes>

When creating or moving volumes the target partition needs not to be specified.
In this case the option "strategy" fixes the algorithm to select a best
partition. The following strategies are currently implemented:

cluster:        select a partition that has already the biggest number of
                volumes of the same kind and is still within the space
                and committment limits.

free+cluster:   select the least full partition that has already volumes
                of the same kind (belongs to the same volset). This is
                the default algorithm.

free:           select the least full partition within the ones defined for that kind.

iobalance:      similar to free, but takes into account the I/O counts of the volumes & partitions

The following strategies will be implemented on request:

access+cluster: select the partition with the least number of accesses
                during the past 24 hours that has already volumes of the
                same kind.

penalty:        select the partition with the least penalty where penalty
                is a function of various AFS parameters derived from
                partition data and volume characteristics.

=item C< Section VOLUMEPATTERNS>

From the volume name a list of matching AFSVolset names is derived. An 
AFSVolset is either a pool or a project. At least one of the AFSVolset
names has to have a matching entry in the [AFSVOLSET] section. If several
entries match, the rightmost one is taken. Important: the patterns are
processed in the order given in the section. The lines contain a valid
perl pattern to match the volume name and a list of corresponding AFSVolset
names. Ay occurence of the $1 string in the volset names is substituted by
the string from the matching perl subexpression in parentheses on the left.
The matching volset names from left to right also denote a inheritance tree
for volsets in the AFSVOLSET section. That means, if certain flags for
a volset (see below) are set, they are inherited by the volsets to the right
of this volset in the VOLUMEPATTERNS line. It also means that volsets to the
right of a given volset are treated as subsets of the volset. This is not
explicitely coded in the module, but applications should respect that.

Example:

      ^project\.(\w+)\b std $1
      ^(\w+)\.scratch\$ scratch $1 $1_s

Given a volume exp.scratch that would match the regex in the second line
above and hence it will belong to the volsets scratch, exp and exp_s.

With the sample definitions in the previous paragraph the flags from the
scratch pool would be applied, e.g. newquota=5000. The allocated space would
count in the calculation of the quota for the volsets exp_s (max quota 5G),
exp (max quota 20G) and scratch (unlimited). The mount point for that volume
would have to be /afs/foo.com/exp or a subdirectory of that directory.

=back

=head1 AUTHORS

Wolfgang Friebel C<Wolfgang.Friebel@cern.ch>

=head1 SEE ALSO

AFS and Vos modules

=cut

