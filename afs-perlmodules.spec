Summary: CERN AFS perlmodules
Name: afs-perlmodules
Version: 1.00
Release: 1%{?dist}
Source: https://gitlab.cern.ch/afs/perlmodules/%{name}-%{version}.tgz
Group: Filesystems
License: GPLv2
BuildArch: noarch
URL: https://gitlab.cern.ch/afs/perlmodules
Vendor: CERN
## for Vos.pm
Requires: %{_sbindir}/vos
## for ConsoleDB.pm
Requires: perl(DBI)
## this would contain DB credentials, so needs to come from somewhere else
Provides: perl(CERNAFS::ConsoleDBconf) = %{version}

%description
Local version of /p/perlmodules == /afs/cern.ch/project/afs/perlmodules

%prep
%setup -q -n %{name}-%{version}

%build
#as requested by rpmlint

%install
mkdir -p $RPM_BUILD_ROOT%{perl_vendorlib}/CERNAFS

touch ConsoleDBconf.pm

# the RPM-based libraries must not find the old AFS locations
# even if these would be in the library path.
sed -i -e '/^use lib .\/afs/d' *.pm

# So we install into a subdir, and refer to via 'CERNAFS::'
# However, some scripts depend on a local GIT checkout,
# so don't commit these changes yet, and do on-the-fly:
sed -i -e 's/^\(\s*package\s\+\)/\1CERNAFS::/' *.pm

sed -i -e 's/^\(\s*use\s\+\)Vos;/\1CERNAFS::Vos;/' \
       -e 's/^\(\s*use\s\+\)ConsoleDBconf;/\1CERNAFS::ConsoleDBconf;/' \
       -e 's/^\(\s*use\s\+\)ConsoleDB;/\1CERNAFS::ConsoleDB;/' \
       -e 's/^\(\s*require\s\+\)ConsoleDB/\1CERNAFS::ConsoleDB/' \
       -e 's/\([$& ]\)ConsoleDB::/\1CERNAFS::ConsoleDB::/g' \
       -e 's/\([$& ]\)ConsoleDBconf::/\1CERNAFS::ConsoleDBconf::/g' \
       -e 's/\([$& ]\)Vol_Do::/\1CERNAFS::Vol_Do::/g' \
       *.pm

install -m 644 *.pm $RPM_BUILD_ROOT%{perl_vendorlib}/CERNAFS/

%clean
rm -rf $RPM_BUILD_ROOT

%files
%{perl_vendorlib}/CERNAFS/Vos.pm
%{perl_vendorlib}/CERNAFS/Volset.pm
%{perl_vendorlib}/CERNAFS/ConsoleDB.pm
%ghost %{perl_vendorlib}/CERNAFS/ConsoleDBconf.pm
%doc README.md

%changelog
