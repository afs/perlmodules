#-----------------------------------------------------------------------------
#
#  Database interface for the CERN AFS Console
#
#  Original version by Jerome Belleman IT-DSS/FDO, August 2010.
#
#-----------------------------------------------------------------------------

package ConsoleDB;

use ConsoleDBconf;
use DBI;
use Time::Local;
use POSIX qw(:signal_h);

use strict;
use warnings;

use constant TIMEOUT => 60;

our $dbh;

sub __connect() {
    my $dsn = "DBI:mysql:database=afsconsole;host=$ConsoleDBconf::db_host";
    my $dbh = DBI->connect($dsn, $ConsoleDBconf::db_username, $ConsoleDBconf::db_passwd);
#    $dbh->{InactiveDestroy} = 1;
    return $dbh;
}

sub __map($) {
    my $_table = shift;
    if (lc $_table eq 'volumes') {
        if (lc $ConsoleDBconf::db_backend eq 'mysql') {
            return (
                nodename                    => 'nodename',
                timestamp                   => 'timestamp',
                volumeid                    => 'volumeid',
                volumename                  => 'volumename',
                server                      => 'server',
                partition                   => 'vice_partition',
                type                        => 'type',
                size                        => 'size',
                files                       => 'files',
                status                      => 'status',
                quota                       => 'quota',
                availability                => 'availability',
                currentaccesses             => 'currentaccesses',
                currentreads                => 'currentreads',
                currentwrites               => 'currentwrites',
                totalaccesses               => 'totalaccesses',
                totalreads                  => 'totalreads',
                totalwrites                 => 'totalwrites',
                project                     => 'project',
                quality                     => 'quality',
                depth                       => 'depth',
                flag                        => 'flag',
                totalRemoteReads            => 'totalRemoteReads',
                totalRemoteWrites           => 'totalRemoteWrites',
                'max(timestamp)'            => 'max(timestamp)',
                'count(*)'                  => 'count(*)',
                'sum(totalaccesses)'        => 'sum(totalaccesses)',
                'sum(totalreads)'           => 'sum(totalreads)',
                'sum(totalwrites)'          => 'sum(totalwrites)'
            );
        } elsif (lc $ConsoleDBconf::db_backend eq 'lemon') {
            return (
                nodename                    => 'nodename',
                timestamp                   => 'timestamp',
                volumeid                    => 'volumes_volumeid',
                volumename                  => 'volumes_volumename',
                server                      => 'volumes_server',
                partition                   => 'volumes_partition',
                type                        => 'volumes_type',
                size                        => 'volumes_size',
                files                       => 'volumes_files',
                status                      => 'volumes_status',
                quota                       => 'volumes_quota',
                availability                => 'volumes_availability',
                currentaccesses             => 'volumes_currentaccesses',
                currentreads                => 'volumes_currentreads',
                currentwrites               => 'volumes_currentwrites',
                totalaccesses               => 'volumes_totalaccesses',
                totalreads                  => 'volumes_totalreads',
                totalwrites                 => 'volumes_totalwrites',
                project                     => 'volumes_project',
                quality                     => 'volumes_quality',
                depth                       => 'volumes_depth',
                flag                        => 'volumes_flag',
                totalRemoteReads            => 'volumes_totalRemoteReads',
                totalRemoteWrites           => 'volumes_totalRemoteWrites',
                'max(timestamp)'            => 'max(timestamp)',
                'count(*)'                  => 'count(*)',
                'sum(totalaccesses)'        => 'sum(volumes_totalaccesses)',
                'sum(totalreads)'           => 'sum(volumes_totalreads)',
                'sum(totalwrites)'          => 'sum(volumes_totalwrites)'
            );
        }
    } elsif (lc $_table eq 'partitions') {
        if (lc $ConsoleDBconf::db_backend eq 'mysql') {
            return (
                nodename            => 'nodename',
                timestamp           => 'timestamp',
                server              => 'server',
                partition           => 'vice_partition',
                fillstatus          => 'fillstatus',
                size                => 'size',
                used                => 'used',
                volumes             => 'volumes',
                online              => 'online',
                offline             => 'offline',
                committed           => 'committed',
                readaccesstime      => 'readaccesstime',
                writeaccesstime     => 'writeaccesstime',
                'max(timestamp)'    => 'max(timestamp)',
                'count(*)'          => 'count(*)'
            );
        } elsif (lc $ConsoleDBconf::db_backend eq 'lemon') {
            return (
                nodename            => 'nodename',
                timestamp           => 'timestamp',
                server              => 'partitions_server',
                partition           => 'partitions_partition',
                fillstatus          => 'partitions_fillstatus',
                size                => 'partitions_size',
                used                => 'partitions_used',
                volumes             => 'partitions_volumes',
                online              => 'partitions_online',
                offline             => 'partitions_offline',
                committed           => 'partitions_committed',
                readaccesstime      => 'partitions_readaccesstime',
                writeaccesstime     => 'partitions_writeaccesstime',
                'max(timestamp)'    => 'max(timestamp)',
                'count(*)'          => 'count(*)'
            );
        }
    }
}

sub __timespan($$$) {
    my ($_mapping, $_timespan, $_first) = @_;

    my $conj;
    if ($_first) {
        $conj = 'WHERE';
    } else {
        $conj = 'AND';
    }
    my $begin = '';
    my $end = '';
    if ($_timespan and $_timespan =~ /^([^\/]+)(\/([^\/]+))?$/) {
        my $tscol = __criterion($_mapping, 'timestamp');
        my $time = __timestamp($1);
        unless ($time) {
            return;
        }
        $begin = "$conj $tscol >= '$time'";
        if ($3) {
            $time = __timestamp($3);
            if ($time) {
                $end = "AND $tscol <= '$time'";
            }
        } else {
            $end = '';
        }
    }

    return "$begin $end";
}

sub __criterion($$) {
    my ($_mapping, $_criterion) = @_;

    if ($_criterion) {
        for (keys %$_mapping) {
            if (lc eq $_criterion) {
                return $_mapping->{$_criterion};
            }
        }
    }
}

sub __criteria($$) {
    my ($_mapping, $_cols) = @_;

    if ($_cols) {
        my @criteria;
        my @groups;
        foreach (split(/\//, $_cols)) {
            if (/(%?)(.*)/) {
                my $criterion = __criterion($_mapping, $2);
                if ($criterion) {
                    push(@criteria, $criterion);
                    if ($1 eq '%') {
                        push(@groups, $criterion);
                    }
                }
            }
        }
        my $what;
        if (@criteria > 0) {
            $what = join(', ', @criteria);
        } else {
            # If there are columns but these do not exist, it is an error
            return;
        }

        my $agg;
        if (@groups > 0) {
            $agg = 'GROUP BY ' . join(', ', @groups);
        }

        return ($what, $agg);
    } else {
        # It's find not to specify any column: default is *
        return ('*', undef);
    }
}

sub __table($$) {
    my ($_table, $_isrecent) = @_;

    if (lc $_table eq 'volumes') {
        if (lc $ConsoleDBconf::db_backend eq 'mysql') {
            return 'volumes';
        } elsif (lc $ConsoleDBconf::db_backend eq 'lemon') {
            if ($_isrecent) {
                return 'm_t_905';
            } else {
                return 'm_l_905';
            }
        }
    } elsif (lc $_table eq 'partitions') {
        if (lc $ConsoleDBconf::db_backend eq 'mysql') {
            return 'partitions';
        } elsif (lc $ConsoleDBconf::db_backend eq 'lemon') {
            if ($_isrecent) {
                return 'm_t_904';
            } else {
                return 'm_l_904';
            }
        }
    }
}

sub __recent() {
    if (lc $ConsoleDBconf::db_backend eq 'mysql') {
        return 'WHERE recent = 1';
    }
}

sub __timestamp($) {
    my $_time = shift;
    
    if ($_time) {
        my ($year, $month, $day, $time, $hour, $min, $sec) =
            $_time =~ /^(\d{4})-(\d{1,2})-(\d{1,2})( (\d{1,2}):(\d{1,2}):(\d{1,2}))?$/;
        return timelocal($sec, $min, $hour, $day, $month - 1, $year);
    }
}

sub __srvprt($$$) {
    my ($_mapping, $_srvprt, $_first) = @_;

    my $conj;
    if ($_first) {
        $conj = 'WHERE';
    } else {
        $conj = 'AND';
    }
    my $server = '';
    my $partition = '';
    if ($_srvprt and $_srvprt =~ /^([^\/]+)(\/([^\/]+))?$/) {
        my $srvcol = __criterion($_mapping, 'server');
        $server = "$conj $srvcol = '$1'";
        if ($3) {
            my $prtcol = __criterion($_mapping, 'partition');
            $partition = " AND $prtcol = '$3'";
        } else {
            $partition = '';
        }
    }
    return "$server$partition";
}

sub __vol($$$) {
    my ($_mapping, $_vol, $_first) = @_;

    my $conj;
    if ($_first) {
        $conj = 'WHERE';
    } else {
        $conj = 'AND';
    }
    my $vol = '';
    if ($_vol) {
        my $volcol;
        if ($_vol =~ /^(\d+)$/) {
            $volcol = __criterion($_mapping, 'volumeid');
        } else {
            $volcol = __criterion($_mapping, 'volumename');
        }
        $vol = "$conj $volcol = '$_vol'";
    }
    return $vol;
}

sub __prj($$$) {
    my ($_mapping, $_prj, $_first) = @_;

    my $conj;
    if ($_first) {
        $conj = 'WHERE';
    } else {
        $conj = 'AND';
    }
    my $prj = '';
    if ($_prj) {
        my $prjcol = __criterion($_mapping, 'project');
        $prj = "$conj $prjcol = '$_prj'";
    }
    return $prj;
}

sub __rownum($$) {
    my ($_rowc, $_first) = @_;

    if (lc $ConsoleDBconf::db_backend eq 'lemon') {
        if ($_rowc and $_rowc =~ /^\d+$/) {
            my $conj;
            if ($_first) {
                $conj = 'WHERE';
            } else {
                $conj = 'AND';
            }
            return "$conj rownum <= $_rowc";
        }
    }
    return '';
}

sub __limit($) {
    my ($_rowc) = @_;

    if (lc $ConsoleDBconf::db_backend eq 'mysql') {
        if ($_rowc and $_rowc =~ /^\d+$/) {
            return "LIMIT $_rowc";
        }
    }
    return '';
}

sub __order($$) {
    my ($_mapping, $_orderby) = @_;

    if ($_orderby) {
        my @criteria;
        foreach (split(/\//, $_orderby)) {
            if (/^([\+-]?)(.*)$/) {
                my $criterion = __criterion($_mapping, $2);
                if ($criterion) {
                    if ($1 eq '-') {
                        push(@criteria, "$criterion DESC");
                    } else {
                        push(@criteria, "$criterion");
                    }
                } else {
                    return;
                }
            }
        }
        return "ORDER BY " . join(', ', @criteria);
    }
    return '';
}

=head1 ConsoleDB

AFS Console General Purpose Programming Interface

=head1 Low-Level Interface

=over

=cut

=item volumes($cols, $srvprt, $volume, $project, $timespan, $orderby, $rowc)

The $cols argument is a slash-separated list of columns to display. All 
columns are assumed if it's left undef.

The $srvprt argument holds a server name, optionally followed by a
slash-prefixed partition name. Any server or partition is assumed if it's
left undef.

The $volume argument holds a volume name or ID. Any volume is assumed if it's
left undef.

The $project argument holds a project name. Any project is assumed if it's
left undef.

The $timespan argument holds a start date, optionally followed by a 
slash-prefixed end date. A date is written in the YYYY-MM-DD format,
optionally followed by a space-prefixed HH:MM:SS-formatted hour of the day.
Only recent samples are considered if the argument is left undef.

The $orderby argument is a slash-separated list of columns given in the
order in which the results are to be sorted. A column prefixed with
a minus (-) sign describes the column is to be sorted in reverse order.

The $rowc argument tells how many records to look for.

Returns an array of volume lines, each referencing the array of 
column values as specified in the $cols argument. Returns undef upon error.

=cut

sub volumes($$$$$$$) {
    my ($_cols, $_srvprt, $_vol, $_prj, $_timespan, $_orderby, $_rowc) = @_;
    my %mapping;

    # Get column mapping from config file
    unless (%mapping = __map('volumes')) {
        return;
    }

    # Validate and map columns and groups
    my ($what, $agg) = __criteria(\%mapping, $_cols);
    if (not defined $what) {
        return;
    }
    if (not defined $agg) {
        $agg = '';
    }

    # Map table
    my $table;
    my $recent = '';
    if ($_timespan) {
        $table = __table('volumes', 0);
    } else {
        $table = __table('volumes', 1);
        $recent = __recent();
    }
    unless ($table) {
        return;
    }

    # Handle server/partition
    my $srvprt;
    if ($recent) {
        $srvprt = __srvprt(\%mapping, $_srvprt, 0);
    } else {
        $srvprt = __srvprt(\%mapping, $_srvprt, 1);
    }

    # Handle timespan
    my $timespan;
    if ($srvprt or $recent) {
        $timespan = __timespan(\%mapping, $_timespan, 0);
    } else {
        $timespan = __timespan(\%mapping, $_timespan, 1);
    }

    # Handle volume
    my $vol;
    if ($srvprt or $recent or $timespan) {
        $vol = __vol(\%mapping, $_vol, 0);
    } else {
        $vol = __vol(\%mapping, $_vol, 1);
    }

    # Handle project
    my $prj;
    if ($srvprt or $recent or $timespan or $vol) {
        $prj = __prj(\%mapping, $_prj, 0);
    } else {
        $prj = __prj(\%mapping, $_prj, 1);
    }

    # Handle rownum (which will apply for Lemon only)
    my $rownum;
    if ($srvprt or $recent or $timespan or $vol or $prj) {
        $rownum = __rownum($_rowc, 0);
    } else {
        $rownum = __rownum($_rowc, 1);
    }

    # Handle sorting
    my $orderby = __order(\%mapping, $_orderby);

    # Handle limit (which will apply for MySQL only)
    my $limit = __limit($_rowc);

    # Connect to global DB if it's not already done
    unless ($ConsoleDB::dbh) {
        $ConsoleDB::dbh = __connect();
        unless ($ConsoleDB::dbh) {
            return;
        }
    }

    my $query = <<EOF;
SELECT $what FROM $table
$recent $srvprt $timespan $vol $prj $rownum $orderby $limit $agg
EOF
    #print "$query\n";
    my $sth = $ConsoleDB::dbh->prepare($query);
    $sth->execute();
    # Note that fetchall_arrayref() never returns undef, even if it's the
    # end of the world.

    return $sth->fetchall_arrayref();
}

=item volume($cols, $srvprt, $volume, $project, $timespan)

The $cols argument is a slash-separated list of columns to display. All 
columns are assumed if it's left undef.

The $srvprt argument holds a server name, optionally followed by a 
slash-prefixed partition name. Any server or partition is assumed if it's left 
undef.

The $volume argument holds a volume name or ID. Any volume is assumed if it is 
left undef.

The $project argument holds a project name. Any project is assumed if it is 
left undef.

The $timespan argument holds a start date, optionally followed by a 
slash-prefixed end date. A date is written in the YYYY-MM-DD format,
optionally followed by a space-prefixed HH:MM:SS-formatted hour of the day.
Only recent samples are considered if the argument is left undef.

Returns an array for a single volume made of the values asked for in
the $cols argument. Returns undef upon error.

=cut

sub volume($$$$$) {
    my ($_cols, $_srvprt, $_vol, $_prj, $_timespan) = @_;
    my %mapping;

    # Get column mapping from config file
    unless (%mapping = __map('volumes')) {
        return;
    }

    # Validate and map columns
    my ($what, $agg) = __criteria(\%mapping, $_cols);
    if (not defined $what) {
        return;
    }
    if (not defined $agg) {
        $agg = '';
    }

    # Map table
    my $table;
    my $recent = '';
    if ($_timespan) {
        $table = __table('volumes', 0);
    } else {
        $table = __table('volumes', 1);
        $recent = __recent();
    }
    unless ($table) {
        return;
    }

    # Handle server/partition
    my $srvprt;
    if ($recent) {
        $srvprt = __srvprt(\%mapping, $_srvprt, 0);
    } else {
        $srvprt = __srvprt(\%mapping, $_srvprt, 1);
    }

    # Handle timespan
    my $timespan;
    if ($srvprt or $recent) {
        $timespan = __timespan(\%mapping, $_timespan, 0);
    } else {
        $timespan = __timespan(\%mapping, $_timespan, 1);
    }

    # Handle volume
    my $vol;
    if ($srvprt or $recent or $timespan) {
        $vol = __vol(\%mapping, $_vol, 0);
    } else {
        $vol = __vol(\%mapping, $_vol, 1);
    }

    # Handle project
    my $prj;
    if ($srvprt or $recent or $timespan or $vol) {
        $prj = __prj(\%mapping, $_prj, 0);
    } else {
        $prj = __prj(\%mapping, $_prj, 1);
    }

    # Connect to global DB if it's not already done
    unless ($ConsoleDB::dbh) {
        $ConsoleDB::dbh = __connect();
        unless ($ConsoleDB::dbh) {
            return;
        }
    }

    my $query = <<EOF;
SELECT $what FROM $table
$recent $srvprt $timespan $vol $prj $agg
EOF
    #print "$query\n";
    my $sth = $ConsoleDB::dbh->prepare($query);
    $sth->execute();
    # Note that fetch() may return undef whenever there's something wrong
    return $sth->fetch();
}

=item partitions($cols, $srvprt, $timespan, $orderby, $rowc)

The $cols argument is a slash-separated list of columns to display. All 
columns are assumed if it's left undef.

The $srvprt argument holds a server name, optionally followed by a
slash-prefixed partition name. Any server or partition is assumed if it's
left undef.

The $timespan argument holds a start date, optionally followed by a 
slash-prefixed end date. A date is written in the YYYY-MM-DD format,
optionally followed by a space-prefixed HH:MM:SS-formatted hour of the day.
Only recent samples are considered if the argument is left undef.

The $orderby argument is a slash-separated list of columns given in the
order in which the results are to be sorted. A column prefixed with
a minus (-) sign describes the column is to be sorted in reverse order.

The $rowc argument tells how many records to look for.

Returns an array of partition lines, each referencing the array of 
column values as specified in the $cols argument. Returns undef upon error.

=cut

sub partitions($$$$$) {
    my ($_cols, $_srvprt, $_timespan, $_orderby, $_rowc) = @_;
    my %mapping;

    # Get column mapping from config file
    unless (%mapping = __map('partitions')) {
        return;
    }

    # Validate and map columns
    my ($what, $agg) = __criteria(\%mapping, $_cols);
    if (not defined $what) {
        return;
    }
    if (not defined $agg) {
        $agg = '';
    }

    # Map table
    my $table;
    my $recent = '';
    if ($_timespan) {
        $table = __table('partitions', 0);
    } else {
        $table = __table('partitions', 1);
        $recent = __recent();
    }
    unless ($table) {
        return;
    }

    # Handle server/partition
    my $srvprt;
    if ($recent) {
        $srvprt = __srvprt(\%mapping, $_srvprt, 0);
    } else {
        $srvprt = __srvprt(\%mapping, $_srvprt, 1);
    }

    # Handle timespan
    my $timespan;
    if ($srvprt or $recent) {
        $timespan = __timespan(\%mapping, $_timespan, 0);
    } else {
        $timespan = __timespan(\%mapping, $_timespan, 1);
    }

    # Handle rownum (which will apply for Lemon only)
    my $rownum;
    if ($srvprt or $recent or $timespan) {
        $rownum = __rownum($_rowc, 0);
    } else {
        $rownum = __rownum($_rowc, 1);
    }

    # Handle sorting
    my $orderby = __order(\%mapping, $_orderby);

    # Handle limit (which will apply for MySQL only)
    my $limit = __limit($_rowc);

    # Connect to global DB if it's not already done
    unless ($ConsoleDB::dbh) {
        $ConsoleDB::dbh = __connect();
        unless ($ConsoleDB::dbh) {
            return;
        }
    }

    my $query = <<EOF;
SELECT $what FROM $table
$recent $srvprt $timespan $rownum $orderby $limit $agg
EOF
    #print "$query\n";
    my $sth = $ConsoleDB::dbh->prepare($query);
    $sth->execute();
    return $sth->fetchall_arrayref();
}

=item partition($cols, $srvprt, $timespan)

The $cols argument is a slash-separated list of columns to display. All 
columns are assumed if it's left undef.

The $srvprt argument holds a server name, optionally followed by a
slash-prefixed partition name. Any server or partition is assumed if it's
left undef.

The $timespan argument holds a start date, optionally followed by a 
slash-prefixed end date. A date is written in the YYYY-MM-DD format,
optionally followed by a space-prefixed HH:MM:SS-formatted hour of the day.
Only recent samples are considered if the argument is left undef.

Returns an array for a single partition made of the values asked for in
the $cols argument. Returns undef upon error.

=back

=cut

sub partition($$$) {
    my ($_cols, $_srvprt, $_timespan) = @_;
    my %mapping;

    # Get column mapping from config file
    unless (%mapping = __map('partitions')) {
        return;
    }

    # Validate and map columns
    my ($what, $agg) = __criteria(\%mapping, $_cols);
    if (not defined $what) {
        return;
    }
    if (not defined $agg) {
        $agg = '';
    }

    # Map table
    my $table;
    my $recent = '';
    if ($_timespan) {
        $table = __table('partitions', 0);
    } else {
        $table = __table('partitions', 1);
        $recent = __recent();
    }
    unless ($table) {
        return;
    }

    # Handle server/partition
    my $srvprt;
    if ($recent) {
        $srvprt = __srvprt(\%mapping, $_srvprt, 0);
    } else {
        $srvprt = __srvprt(\%mapping, $_srvprt, 1);
    }

    # Handle timespan
    my $timespan;
    if ($srvprt or $recent) {
        $timespan = __timespan(\%mapping, $_timespan, 0);
    } else {
        $timespan = __timespan(\%mapping, $_timespan, 1);
    }

    # Connect to global DB if it's not already done
    unless ($ConsoleDB::dbh) {
        $ConsoleDB::dbh = __connect();
        unless ($ConsoleDB::dbh) {
            return;
        }
    }

    my $query = "SELECT $what FROM $table $recent $srvprt $timespan $agg";

    #print "$query\n";
    my $sth = $ConsoleDB::dbh->prepare($query);
    $sth->execute();
    return $sth->fetch();
}



=head1 Mid-Level Interface

=head2 A Few Sample Mid-Level Functions

=over

=item partition_readaccesstime($srvprt)

=cut

sub partition_readaccesstime($) {
    my $_srvprt = shift;

    unless ($_srvprt) {
        return;
    }

    my $rat = partition('readaccesstime', $_srvprt, undef);

    if ($rat) {
        return $rat->[0];
    }
}


=item partition_writeaccesstime($srvprt)

=cut

sub partition_writeaccesstime($) {
    my $_srvprt = shift;

    unless ($_srvprt) {
        return;
    }

    my $wat = partition('writeaccesstime', $_srvprt, undef);

    if ($wat) {
        return $wat->[0];
    }
}



=item volume_lastupdate($srvprt, $volume)

=cut

sub volume_lastupdate($$) {
    my ($_srvprt, $_vol) = @_;

    my $max = volume('max(timestamp)', $_srvprt, $_vol, undef, undef);

    if ($max) {
        return $max->[0];
    }
}

=item partition_lastupdate($srvprt)

Returns the number of recent volumes whose access times > $threshold

=cut

sub partition_lastupdate($) {
    my $_srvprt = shift;

    my $max = partition('max(timestamp)', $_srvprt, undef);

    if ($max) {
        return $max->[0];
    }
}

=item volumes_countbusy($threshold, $srvprt)

The first argument is compulsory, the second one isn't
Returns the number of recent volumes whose access times > $threshold

=cut

sub volume_countbusy($$) {
    my ($_threshold, $_srvprt) = @_;

    unless (defined $_threshold) {
        return;
    }

    # List all accesses
    my $accesses = volumes('currentaccesses',
                           $_srvprt,
                           undef,
                           undef,
                           undef,
                           '-currentaccesses',
                           undef);

    if (defined $accesses) {
        my $c = 0;
        foreach (@$accesses) {
            if ($_->[0] > $_threshold) {
                $c++;
            } else {
                last;
            }
        }
        return $c;
    }
}

=item volumes_sumtotalaccesses($srvprt)

=cut

sub volume_sumtotalaccesses($) {
    my ($_srvprt) = @_;

    unless (defined $_srvprt) {
        return;
    }

    my $sum = volumes('sum(totalaccesses)',
                      $_srvprt,
                      undef,
                      undef,
                      undef,
                      undef,
                      undef);
    if (defined $sum) {
        foreach (@$sum) {
            return $_->[0];
        }
    }
}

=item volume_activity($volume, $starttimestamp, $endtimestamp)

=back

=cut

sub volume_activity($$$) {
    my ($_vol, $_start, $_end) = @_;

    my $events = volumes('sum(totalaccesses)/sum(totalreads)/sum(totalwrites)',
                         undef,
                         $_vol,
                         undef,
                         "$_start/$_end",
                         undef,
                         undef);

    #print "addr1: $events\n";
    #my @foo = @$events;
    #print "this is " . @foo . "\n";
    #print "addr2: $foo[0]\n";
    #my $rline = $foo[0];
    #my @aline = @$rline;
    #print "this is " . @aline . "\n";
    #print "first value is " . $aline[0] . "\n";

    # If there are no events, SUMs might be NULL
    #foreach (@{$$events[0]}) {
    #    unless (defined) {
    #        return [];
    #    }
    #}


    if (defined $events) {
        return (defined $$events[0][0] && $$events[0][0] > 0 ? 1 : 0,
                defined $$events[0][1] && $$events[0][1] > 0 ? 1 : 0,
                defined $$events[0][2] && $$events[0][2] > 0 ? 1 : 0);
    }
}

#
# Sample utility functions for the ATLAS use case
#
sub partition_isslow($) {
    my $_srvprt = shift;

    unless ($_srvprt) {
        return;
    }

    my $rat = partition('readaccesstime', $_srvprt, undef);
    if ($rat) {
        if ($rat->[0] > 40000) {
            return 1;
        } else {
            return 0;
        }
    }
}

sub volume_isro($$) {
    my ($_srvprt, $_vol) = @_;

    my $type = volume('type', $_srvprt, $_vol, undef, undef);

    if ($type) {
        if ($type->[0] eq 'RW') {
            return 0;
        } else {
            return 1;
        }
    }
}

sub volume_hottestro($) {
    my $_srvprt = shift;

    unless ($_srvprt) {
        return;
    }
    my $vols = volumes('volumeid/currentaccesses',
                       $_srvprt,
                       undef,
                       undef,
                       undef,
                       '-currentaccesses',
                       10);

    foreach (@$vols) {
        if ($_->[1] == 0) {
            last;
        }
        if (volume_isro($_srvprt, $_->[0])) {
            return $_->[0];
        }
    }
}

sub volume_replicac($) {
    my $_vol = shift;

    unless ($_vol) {
        return;
    }
    my $vols = volumes('type', undef, $_vol, undef, undef, undef, undef);
    my $c = 0;
    foreach (@$vols) {
        $c++;
    }
    return $c;
}

### SLS ########################################################################

=head2 SLS Interface

=over

=item server_lastupdates

      Returns a possibly empty server/timestamp hash.

=cut

sub server_lastupdates() {
    my %updates;
    my $srvs = volumes('%server/max(timestamp)', undef, undef, undef, 
                       undef, undef, undef);

    foreach (@$srvs) {
        $updates{$_->[0]} = $_->[1];
    }
    return %updates;
}

=item server_problems($servers)

      Returns a possibly empty array of problematic servers.

=back

=cut

sub server_problems($) {
    my $_servers = shift;

    my @problems;
    foreach (@$_servers) {
        my $problem =
            volumes('volumename/availability/server/partition/timestamp',
                    $_, undef, undef, undef, undef, undef);
        foreach (@$problem) {
            push(@problems, $_);
        }
    }
    return @problems;
}

### Pool Monitor ###############################################################

# General-purpose function wrapped by the few following ones
sub any_access($$$$) {
    my ($_context, $_col, $_srvprtvol, $_span) = @_;

    my $from;
    if (defined $_span) {
        my ($sec, $min, $hour, $mday, $mon, $year) =
            localtime(time - $_span * 3600);
        $mon++;
        $year += 1900;
        $from = "$year-$mon-$mday $hour:$min:$sec";
    } else {
        $from = undef;
    }

    # Setup a timeout
    my $mask = POSIX::SigSet->new(SIGALRM);
    my $action = POSIX::SigAction->new(
        sub { die "DB operation timed out\n" },
        $mask
    );
    my $oldaction = POSIX::SigAction->new();
    sigaction(SIGALRM, $action, $oldaction);

    my $accesses;
    eval {
        eval {
            alarm(TIMEOUT);
            if ($_context eq 'volumes'){
                $accesses = volumes($_col, undef, $_srvprtvol, undef,
                                       $from, undef, undef);
            } elsif ($_context eq 'partitions') {
                $accesses = partitions($_col, $_srvprtvol, $from, undef, undef);
            } else {
                return -1;
            }
        };
        alarm(0);
    };
    sigaction(SIGALRM, $oldaction);

    if (defined $accesses) {
        my $sum = 0;
        my $cnt = 0;
        foreach (@$accesses) {
            $sum += $_->[0];
            $cnt++;
        }

        if ($cnt > 0) {
            return $sum / $cnt;
        } else {
            return -1;
        }
    } else {
        return -1;
    }
}

=head2 Pool Monitor Interface

=over

=item partitions_readaccesstime($srvprt, $span)

      Get the current read access time of a partition. Returns the 
      current access time or <0 upon error.

=cut

sub partitions_readaccesstime {
    my ($_srvprt, $_span) = @_;

    return any_access('partitions', 'readaccesstime', $_srvprt, $_span);
}

=item partitions_writeaccesstime($srvprt, $span)

      Get the current write access time of a partition. Returns the 
      current access time or <0 upon error.

=cut

sub partitions_writeaccesstime {
    my ($_srvprt, $_span) = @_;

    return any_access('partitions', 'writeaccesstime', $_srvprt, $_span);
}

=item volumes_readaccessrate($vol, $span)

      Get the current read access rate of a volume. Returns the 
      current access rate or <0 upon error.

=cut

sub volumes_readaccessrate {
    my ($_vol, $_span) = @_;

    return any_access('volumes', 'currentreads', $_vol, $_span);
}

=item volumes_writeaccessrate($vol, $span)

      Get the current write access rate of a volume. Returns the 
      current access rate or <0 upon error.

=cut

sub volumes_writeaccessrate {
    my ($_vol, $_span) = @_;

    return any_access('volumes', 'currentwrites', $_vol, $_span);
}

=item volumes_accesses($vol, $span)

      Get the total accesses of a volume. Returns the current total 
      accesses or <0 upon error.  

=cut

sub volumes_accesses($) {
    my ($_vol) = @_;

    return any_access('volumes', 'totalaccesses', $_vol, undef);
}

# General-purpose function wrapped by the few following ones
sub volumes_busy($$$) {
    my ($_col,$_srvprt,$_rowc) = @_;


    # Setup a timeout
    my $mask = POSIX::SigSet->new(SIGALRM);
    my $action = POSIX::SigAction->new(
        sub { die "DB operation timed out\n" },
        $mask
    );
    my $oldaction = POSIX::SigAction->new();
    sigaction(SIGALRM, $action, $oldaction);

    my $busy;
    eval {
        eval {
            alarm(TIMEOUT);
            $busy = volumes("timestamp/volumename/$_col", $_srvprt, undef, 
                            undef, undef, "-$_col", undef);
        };
        alarm(0);
    };
    sigaction(SIGALRM, $oldaction);

    my $ths = time - 7200;
    if (defined $busy) {
        my @parts;
        my $n = 0;
        foreach (@$busy) {
            if ($n < $_rowc) {
                if ($_->[0] > $ths and $_->[2] > 0) {
                    push(@parts, $_->[1]);
                    $n++;
                }
            } else {
                last;
            }
        }
        return @parts;
    }
}

=item volumes_accessbusy($srv, $rowc)

      Get the busiest volumes on a server. Returns an array of the
      busiest volumes or undef upon error.

=cut

sub volumes_accessbusy($$) {
    my ($_srv,$_rowc) = @_;

    return volumes_busy('currentaccesses', $_srv, $_rowc);
} 

=item volumes_readbusy($srv, $rowc)

      Get the busiest read volumes on a server. Returns an array of 
      the busiest volumes or undef upon error.

=cut

sub volumes_readbusy($$) {
    my ($_srv,$_rowc) = @_;

    return volumes_busy('currentreads', $_srv, $_rowc);
}

=item volumes_writebusy($srv, $rowc)

      Get the busiest write volumes on a server. Returns an array of 
      the busiest volumes or undef upon error.

=back

=cut

sub volumes_writebusy($$) {
    my ($_srv,$_rowc) = @_;

    return volumes_busy('currentwrites', $_srv, $_rowc);
}

1;
