AFS perl modules (from a collaboration between CERN and DESY)

Installed in several locations:
* locally on `afssrv` machines, either
    * through puppet, from GIT, into `/opt/perlmodules/` ("qa" or "master" branches)
    * as RPM
    * note: this contains a subset of modules which are known to be used
* centrally on `/afs/cern.ch/project/afs/perlmodules/` "master" branch, (replicated)
    * this contains also ancient cruft where usage is unclear (and which the GIT repository ignores)

----
Created via
```
rcs-fast-export.rb \
  --authors ~/AFS_authors.txt \
  /afs/cern.ch/project/afs/dev/monitoring/ConsoleDB/ConsoleDB.pm \
  /afs/cern.ch/project/afs/dev/afs_admin/Vos.pm \
  /afs/cern.ch/project/afs/dev/afs_admin/Volset.pm |\
     git fast-import
git reset
```
Adding new modules needs to redo that on an empty repo.

Use `make rpm` to get a (noarch) RPM out of this.
