#!/usr/bin/perl
#
use strict;
use warnings;
INIT {
  # we cannot rely on the AFS-based configuration here
  # the config file needs to exists but can be empty
  $CERNAFS::Volset::CFLOC = "/dev/null";
}
use CERNAFS::Volset;
use CERNAFS::Vos;
# Vos::_print but should be exported?
_print("CERNAFS:: perl modules loaded OK");
1;
